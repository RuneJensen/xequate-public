import sys

sys.path.append('..')
from xequate_data_publish import Publisher

if __name__ == '__main__':
	assert len(sys.argv) == 2, '1 parameter, the path to the configuration, must be provided'
	configFilepath = sys.argv[1]

	Publisher(configFilepath).publishUnits()		

