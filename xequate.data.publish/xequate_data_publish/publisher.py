from xequate_core import ConfigExpert, LogExpert
from xequate_data import UnitExpert, CurrencyExpert
import json
import requests

# firebase

class Publisher:
#	firebaseKeyCharEscapes = None

	def __init__(self, configFilepath, log=None):
		with open('../../xequate.core/firebaseKeyEscapes.json') as file:	
			self.firebaseKeyCharEscapes = json.load(file)

		self.log = (log if log is not None else LogExpert.getConsoleLogger())

		self.log.info('getting configuration..')
		self.config = ConfigExpert(log=self.log).getConfigFromFilename(configFilepath); #'../../xequate.debug.config.json')


	def __escapeFirebaseInvalidKeyChars(self, objJson):
		for key in objJson.keys():
			value = objJson[key]
			if type(value) is dict:
				objJson[key] = self.__escapeFirebaseInvalidKeyChars(value)
			newKey = key			
			for toReplace in self.firebaseKeyCharEscapes:
				replacement = self.firebaseKeyCharEscapes[toReplace]
				newKey = newKey.replace(toReplace, replacement)
			if newKey != key:
				objJson[newKey] = objJson[key]
				del objJson[key]

		return objJson


	def __firebaseInsertOrUpdate(self, firebaseAppId, firebaseAuthKey, dataJson, writeLocations): 
		# Store rates to NoSQL provider
		for (index, writeLocation) in enumerate(writeLocations): 
			self.log.info('Storing data to location {0}/{1} (\'{2}\')..'.format(index +1, len(writeLocations), writeLocation))
			
			"""
			firebaseUrl = '..url..'
			db = firebase.FirebaseApplication(firebaseUrl, None)
					
			print 'Authenticating..'
			db.authentication = firebase.FirebaseAuthentication('.......auth........', None)
			
			
			#result = db.post('/rates/%s' % timestamp, None, None, None) #{'print': 'pretty'}
			result = db.put(url='/rates', name=timestamp, data=dataJson, connection=None, params={'print': 'pretty'}, headers={'X_FANCY_HEADER': 'VERY FANCY'})
			"""
			
			
			#postDataStr = '{ "name": ".." }' #urllib.urlencode(jsonData)
			
			postUrl = 'https://{0}.firebaseio.com/{1}.json?auth={2}'.format(firebaseAppId, writeLocation, firebaseAuthKey) 
			dataJson = self.__escapeFirebaseInvalidKeyChars(dataJson)
			postDataStr = json.dumps(dataJson) # urllib.urlencode(jsonData)

			r = requests.put(postUrl, data=postDataStr) # put request - creates or updates
			responseStr = r.content

			responseJson = json.loads(responseStr)

			isResponseError = 'error' in responseJson
			if isResponseError:
				self.log.debug('sent: \n' + postDataStr)
				self.log.debug('response: \n' + responseStr)

			assert not isResponseError, 'response json had an \'error\' key'
			assert responseJson == dataJson, 'response json did not equal what was sent'

			#return result		
		
		return 'ok'

	def __publish(self, categoriesToPost):
		self.log.info('category count to post: {0}'.format(len(categoriesToPost)))
		
		KEY_NAME = 'name'

		names = ', '.join(category[KEY_NAME] for category in categoriesToPost)
		self.log.info('category names to publish:\n\t{0}'.format(names))

		for category in categoriesToPost:	
			postParentKey = category[KEY_NAME].lower().replace(' ', '_');
			timestamp = category['timestamp']

			self.__firebaseInsertOrUpdate( \
				self.config['firebase_app_id'], \
				self.config['firebase_auth_key'], \
				category, \
				['latest/{0}'.format(postParentKey), 'history/{0}/{1}'.format(timestamp, postParentKey)] \
				)


	def publishUnits(self):
		self.log.info('units publish begin')

		self.log.info('getting unit categories..')
		categoriesToPost = UnitExpert(log=self.log).getUnitJson()

		self.__publish(categoriesToPost)	
		self.log.info('units publish end')


	def publishCurrencies(self):
		self.log.info('currency publish begin')

		self.log.info('getting currency category..')
		categoriesToPost = [CurrencyExpert(log=self.log).getCurrencyJson(self.config['open_exchange_app_id'])]

		self.__publish(categoriesToPost)	
		self.log.info('currency publish end')


	def publishAll(self):
		self.publishUnits()
		self.publishCurrencies()


# if __name__ == '__main__':
#     Publisher().publishAll()






