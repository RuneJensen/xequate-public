'use strict';

module.exports = (function () {
	return {
		    //buildFiles: 'src/**/*.ts',
		    serveDir: __dirname + '/build',
		    transpileFiles: 'build/**/*.ts',
		    buildDir: './build',
		    tempDir: './tmp',
		    appFilename: 'app.js',
		    entryFilename: 'bootstrap.js',
		    isRelease: false,
		    //ignoreModulesNames: 'angular2/angular2',  // TODO: ignore when bugfix arrives
		    devPort: 1234,
	}
})();