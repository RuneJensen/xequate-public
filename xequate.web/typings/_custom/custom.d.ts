declare var require; // is made available through webpack


declare var CONFIGURATION: {
	FIREBASE_APP_ID: string;
	BUILD_TYPE: string;
	BUILD_MAJOR_VERSION: number;
	BUILD_MINOR_VERSION: number;
	DEFAULT_LEFT_CODE: string;
	DEFAULT_RIGHT_CODE: string;	
	getIsConsoleOutputEnabled(): boolean;
}

