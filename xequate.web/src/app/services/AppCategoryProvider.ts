/// <reference path="../../../typings/tsd.d.ts" />

import {Inject} from 'angular2/angular2';
import {Logger, AppLogger} from '../../xequate/log';
import {FirebaseGateway} from './FirebaseGateway';
import {CategoryProvider} from '../../xequate/data';
import {AppDataGateway} from './AppDataGateway';

// TODO: rename to DICategoryProvider + replace later with DI 
@Inject(AppDataGateway)
export class AppCategoryProvider extends FirebaseGateway {
    protected log: Logger = new AppLogger(this);


	constructor(dataGateway: AppDataGateway) {
		super(dataGateway);

		this.log.v('construct');
	}

}