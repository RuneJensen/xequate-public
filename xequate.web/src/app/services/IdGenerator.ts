import {Logger} from '../../log/Logger';
import {AppLogger} from '../../log/AppLogger';

export class IdGenerator {
    private log: Logger = new AppLogger(this);

    private idCount = 0;

	constructor() {
		this.log.v('construct');
	}

	getUniqueId(): string {
		this.idCount++;

		return 'unique-id-' + this.idCount;
	}

	getPseudoGuid(): string {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    		var r = Math.random() * 16|0, 
    			v = c == 'x' ? r : (r&0x3|0x8);

    		return v.toString(16);
		});

	}
}