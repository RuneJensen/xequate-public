/// <reference path="../../../../typings/tsd.d.ts" />

var path = require('path');
var fs = require('fs');

var xpath = require('xpath');
var XmlDocument = require('xmldom').DOMParser;


module.exports = function(dummy) {
  this.cacheable();

  var dir = path.dirname(this.resourcePath);
  var countriesFilepath = path.normalize(dir + '/../resource/xml/ISO3166.xml');
  var currencyFilepath  = path.normalize(dir + '/../resource/xml/ISO4217.xml');
  
  this.addDependency(countriesFilepath);
  this.addDependency(currencyFilepath);

  var xmlToDocument = function(xml) { return new XmlDocument().parseFromString(xml); }

  var getChildTextMaybe = function(childName, parent) {
    var childNodeText = xpath.select1(childName + '/text()', parent);
    if (childNodeText == undefined)
      return undefined;

    return childNodeText.toString();
  }

  var countryMap = {};
  var getOrCreateCountryById = function(id) {
    if (countryMap[id] == undefined)
      countryMap[id] = { id: id, code: undefined, names: [] };

    return countryMap[id];
  }

  var currencyMap = {};
  var getOrCreateCurrencyByCode = function(code) {
    code = code.toUpperCase();

    if (currencyMap[code] == undefined)
      currencyMap[code] = { code: code, name: undefined, countries: []};

    return currencyMap[code];
  }

  var countryXmlString  = fs.readFileSync(countriesFilepath, 'utf8');
  var currencyXmlString = fs.readFileSync(currencyFilepath,  'utf8');

  var countryDoc = xmlToDocument(countryXmlString);
  var currencyDoc = xmlToDocument(currencyXmlString);

  (function processCurrencyNodes() {
    var currencyNodes = xpath.select('//CcyNtry', currencyDoc);
    for(node of currencyNodes) {
      var currencyCode = getChildTextMaybe('Ccy', node);
      var currencyName = getChildTextMaybe('CcyNm', node); 
      var countryName = getChildTextMaybe('CtryNm', node); 
      var countryId = getChildTextMaybe('CcyNbr', node); 

      if (currencyCode != undefined) {
        var currency = getOrCreateCurrencyByCode(currencyCode);
        if (currency.name == undefined)
          currency.name = currencyName;

        var country = getOrCreateCountryById(countryId);
        if (country.names.indexOf(countryName) < 0)
          country.names.push(countryName);

        if (currency.countries.indexOf(country) < 0)
          currency.countries.push(country);
      } 
    }
  })();

  (function processCountryNodes(){
    var countryNodes = xpath.select("//country", countryDoc);
    for(var node of countryNodes) {
      var countryName  = xpath.select1('@name', node).value;
      var countryId  = xpath.select1('@country-code', node).value;
      var countryCode  = xpath.select1('@alpha-2', node).value;

      var country = getOrCreateCountryById(countryId);
      if (country.names.indexOf(countryName) < 0)
        country.names.unshift(countryName); // add to first position, because these names are prefereable

      country.code = countryCode;
    }
  })();
  
  (function applySpecialCases(){
    getOrCreateCurrencyByCode('EUR').countries[0].code = 'EU';
    
    var bitcoin = getOrCreateCurrencyByCode('BTC')
    bitcoin.name = 'Bitcoin'
    bitcoin.countries.push({ id: -1, code: 'BTC', names: ['Digital Currency'] })// [0].code = 'BTC';
  })();

  return JSON.stringify(currencyMap);
}