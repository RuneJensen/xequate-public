/// <reference path="../../../../typings/tsd.d.ts" />

var glob = require('glob');      
var path = require('path');
var sizeOf = require('image-size');
var fs = require('fs');

var imagePath = '/../resource/images/32';
var imageExtension = 'png';


module.exports = function(dummy) {
  this.cacheable();

  var dir = path.dirname(this.resourcePath);
  var entries = [];
  var filesPath = path.normalize(dir + imagePath) + '/*' + imageExtension;
  var files = glob.sync(filesPath);
  for(var index = 0; index < files.length; index++) {
    var file = files[index];
    this.addDependency(file);
 
    var className = path.basename(file, '.' + imageExtension);
    var base64encodedImage = fs.readFileSync(file, { encoding: 'base64' });
    var imgDim = sizeOf(file)

    var entry = ".country-" + className + "{" +
      "width: " + imgDim.width + "px;" + 
      "height: " + imgDim.height + "px;" +
      "background-repeat: no-repeat;" +
      "background-image: url(data:image/" + imageExtension + ";base64," + base64encodedImage + ');' +    //R0lGODlhUAAPANUAAAAAAAoKChQUFBoaGiQkJCwsLD4+PkJCQk1NTVNTU15eXmJiYmxsbHJycnt7e4ODg4iIiJZsbJqamqKioqqqqqxERLS0tL+/v8PDw8zMzNTU1Nvb2+Tk5O3t7fPz8/7+/v8AAP8ICP8WFv8cHP8jI/8yMv86Ov9CQv9KSv9VVf9lZf9ycv97e/+Dg/+Jif+Wlv+bm/+rq/+ysv+5uf/IyP/Q0P/f3//j4//19QAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAkAADkALAAAAABQAA8AAAb/wENjSCwaj8ikcskcVpSHx2dKrVqv2Kx2y52eZtmHtEsum8klEg4rnnI8007nI6/PPXDOnD7XxzlngVQkICxUNjA1H20cAApTBQMZAJSVGAMFHgAIUwYBHwMBcKFwgmciICAvKoQpU4yUCx2RkwQItwgamJqOHZ4fDQAWkwmmZjgzqcogJWuLUo2UAgGSAA4dF9keu5UB0x8YAAwPABTGXDQtJyHLqSI3VLAJDpTVDhaW3AgPAQCfoJREndsSA0U7ZTGqwFrwYYMAexscHABwKROAYhsIfOoAYAABABsGcrnBYkQ7V/GgAWD4gZa1VxS5FfvwSwIACBMAjBG5hcZBcBkpPzRi6bISpYqaZv4yAEAPAAI8uSQDYaKFyRHO2viCMGVBgg0GworVkGCBBwMOpjBA4OEASwYGAEXNEgPECHgfZKBY8Wrn3L9cYICgYQXvA66AE2txAUMLhAMQIkuWLKay5cuYM2vezNlyBM4HggAAOw==);" // + "" + //url('~!!url?limit=1024!#{file}');
      "}";
    entries.push(entry);
  }
  
 // entries.push('body { border: solid 5px blue; }');

  return entries.join('\n');
}



