/// <reference path="../../../../typings/tsd.d.ts" />

import {Logger} from '../../../log/Logger';
import {AppLogger} from '../../../log/AppLogger';

// make webpack 
require.ensure(
	['!!style?singleton!raw!./loaders/images-as-css-loader!./loaders/images-as-css-loader'], 
	() => require('!!style?singleton!raw!./loaders/images-as-css-loader!./loaders/images-as-css-loader')
	);

// inject the images as base64-encoded style resources
// require('!!style?singleton!raw!./loaders/images-as-css-loader!./loaders/images-as-css-loader');

// get a map of currency code info
let currencyMap = <CurrencyMap>require('!!json!./loaders/country-data-loader!./loaders/country-data-loader');

interface Currency { 
	name: string,
	code: string
	countries: Array<{ name: string, code: string }>;
}

interface CurrencyMap {
	[index: string]: Currency
}

// TODO: extract currency info to separate service
export class CurrencyImageService {
    protected log: Logger = new AppLogger(this);
    //private currencyInfoMap = JSON.parse(currencyInfoMapStr);

    private styleClassPrefix = 'country-';


	constructor() {
		this.log.v('construct');	

		// console.log('$#######################');
		// console.log(JSON.stringify(currencyMap['EUR']));
		// console.log('$#######################');
	}

	getCurrencyName(currencyCode: string): string {
		var currency: Currency = currencyMap[currencyCode];
		if (currency == undefined)
			return undefined;

		return currency.name;
	}

	getCountryImageClass(countryCode: string): string {
		return this.styleClassPrefix + countryCode;
	}

	getCountryImageClassByCurrencyCode(currencyCode: string): string {
		var currency: Currency = currencyMap[currencyCode];
		var doesMappingExist = (currency != undefined && currency.countries.length > 0 && currency.countries[0].code != undefined);
		
		var countryCode: string = (doesMappingExist === true)? currency.countries[0].code : currencyCode.substr(0,2);
			
		if (doesMappingExist === false)	{
			this.log.v('no entry found for currency code \'{0}\', defaulting to \'{1}\'', currencyCode, countryCode);
			return '';
		}

		return this.getCountryImageClass(countryCode);
	}
}