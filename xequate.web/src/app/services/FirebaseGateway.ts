/// <reference path="../../../typings/tsd.d.ts" />

import {EventEmitter} from 'angular2/angular2';
import {DataGatewayState} from '../../data/DataGatewayState';
import {Logger, AppLogger} from '../../xequate/log'
import {State} from '../../data/State';
import {EnumState} from '../../data/EnumState';
import {StringExpert, TypeExpert} from '../../xequate/util';
import {Category, CategoryItem, CategoryProvider, CategoryData} from '../../xequate/data';
import {AppDataGateway} from '../../xequate/service';

const FIREBASE_URL = 'https://' + CONFIGURATION.FIREBASE_APP_ID + '.firebaseio.com';


//@Inject(Serializer)
export class FirebaseGateway implements CategoryProvider {
    protected log: Logger = new AppLogger(this);

    states = DataGatewayState;

    private _state: State<DataGatewayState> = new EnumState<DataGatewayState>(DataGatewayState);

    currencyDataUpdated: EventEmitter = new EventEmitter();
    data: CategoryData = {
        categories: [], // Array<Category>
        categoryKeys: [] // Array<CategoryItem>
    }

    isDataFetchInError: boolean = false;


    constructor(private dataGateway: AppDataGateway) {
        this.log.v('construct');
        //this.log.d(`construct (isStorageAvailable: ${this.isStorageAvailable})`);
        
        this.initiateRequestForLatestData();
    }

    private initiateRequestForLatestData(): void {
        this.log.d('Requesting data..');

        var dataProviderRef = new Firebase(FIREBASE_URL);
        if (this.restoreFromCache() === true) {
            this.state.add(DataGatewayState.HasCachedData);
            this.notifyCurrencyAvailable();
        }

        //ratesStore.orderByKey().limitToLast(1).on(
        this.state.add(DataGatewayState.IsUpdatingData);
        dataProviderRef.child('latest').on(
            'value',
            //'child_added',
            (response) => {
                this.data.categories = [];
                this.data.categoryKeys = [];

                var reponseCategories: Object = response.val();

                this.decodeCharacterEscapes(reponseCategories);

                for(let categoryKey in reponseCategories) {
                   var category = reponseCategories[categoryKey];
                   this.data.categories.push(category);

                    var categoryItemKeys: Array<string> = Object.keys(category.map);
                    categoryItemKeys.forEach(
                        key => this.data.categoryKeys.push({
                            key: key,
                            category: category
                            })
                        );
                }

                this.log.info('data.categories ({0} total): {1}', this.data.categories.length, this.data.categories.map(category => category.name).reduce((aggregate, toAdd) => aggregate + ',' + toAdd));
                this.log.info('aggregated category item count: ' + this.data.categoryKeys.length);

                this.log.debug('storing cache data..');
                this.storeToCache();

                this.state.remove(DataGatewayState.IsUpdatingData);
                this.state.add(DataGatewayState.HasUpdatedData);

                this.notifyCurrencyAvailable();
            },
            (error: Error) => {
                this.log.d(`firebase callback failed: ${error}`);

                this.state.remove(DataGatewayState.IsUpdatingData);
                this.state.add(DataGatewayState.DataFetchFailed);
            }
            );
    }

    private notifyCurrencyAvailable(): void {
        this.currencyDataUpdated.next(new Event('currencyDataUpdated'));    
    }

    getFirstCategoryByKey(key: string): CategoryItem {
        for (let category of this.data.categoryKeys)
            if (StringExpert.areEqual(category.key, key, false) === true)
                return category;

        return undefined;
    }

    private decodeCharacterEscapes(parent) {
        for(let key in parent) {
            var newKey = key;

            var replacements = key.match(/(?!{:)\d+(?=})/g);
            if (replacements != undefined) 
                for(let replacement of replacements) {
                    var charCode: number = parseInt(replacement);
                    newKey = newKey.replace("{:" + charCode + "}", String.fromCharCode(charCode));
                }

            if (newKey !== key) {
                this.log.d('replacing key \'{0}\' with \'{1}\'', key, newKey);
                parent[newKey] = parent[key]
                delete parent[key];
            }

            var child: any = parent[newKey];
            if (TypeExpert.isObject(child))
                this.decodeCharacterEscapes(child);
        }
    }

    doesCategoryKeyExist(key: string): boolean {
        return (this.getFirstCategoryByKey(key) != undefined);
    }

    private restoreFromCache(): boolean {
        var cachedData: CategoryData = this.dataGateway.categoryData;

        if (cachedData == undefined)
            return false;
    
        this.data = cachedData;
        return true;
    }

    private storeToCache(): void {
        this.dataGateway.categoryData = this.data;
    }

    get categories(): Array<Category> {
        return this.data.categories;
    }

    get categoryKeys(): Array<CategoryItem> {
        return this.data.categoryKeys;
    }

    get state(): State<DataGatewayState> { return this._state; } 

    get isCurrencyDataAvailable() {
        return this.state.contains(DataGatewayState.HasCachedData) || this.state.contains(DataGatewayState.HasUpdatedData);
    }

}
