/// <reference path="../../../../typings/tsd.d.ts" />

var glob = require('glob');      
var path = require('path');
var sizeOf = require('image-size');
var fs = require('fs');
var loaderUtils = require("loader-utils");

// var imagePath = '/../resource/images/32';


module.exports = function(input) {
  this.cacheable();

  var config;
  var mappings;
  var applyStyle;
  try {
    config = JSON.parse(input);
    mappings = config['files'];
    applyStyle = config['style'];
  }
  catch(e) {
  }

  // throw new Error(config)
  var query = loaderUtils.parseQuery(this.query);


  var inputIsMappingJson = (config != undefined);
  if (inputIsMappingJson === false) {
    throw new Error('not implemented');
 
    // if (query.path == undefined)
    //   throw new Error('Query parameter \'path\' must be defined when input is not an explicit mapping');
    // var imagePath = query.path;

    // if (query.mapPattern == undefined)
    //   throw new Error ('Query parameter \'mapPattern\' must be defined when input is not an explicit mapping');
    // var mapPattern = query.mapPattern;

    // var imageExtension = (query.extension != undefined)? query.extension : '*';

    // var filesDir = path.normalize(path.dirname(this.resourcePath) + imagePath);  //  path.dirname(this.resourcePath);
    // console.log('!!!!!!!!!!!!!!!!!!!!');
    // console.log(filesDir);  
    // var filesPath = filesDir + '/*.' + imageExtension;

    // var files = glob.sync(filesPath);
    // if (files.length === 0)
    //   throw new Error('No files matched for files pattern \'' + filesPath + '\'');

    // var mappings = {};
    // for(var index = 0; index < files.length; index++) {
    //   var filename = files[index];
    //   mappings[file] = mapPattern;
    // }
  }

  var styleClassChunks = [];
  for(var key in mappings) {
    var filepath = path.normalize(path.dirname(this.resourcePath) + '/' + key);
    var extension = path.extname(filepath);

    styleClassName = mappings[key]
      .replace(
        '[name]', 
        path.basename(filepath, extension)
        );  

    this.addDependency(filepath);
 
    //var className = path.basename(file, '.' + imageExtension);
    var base64encodedImage = fs.readFileSync(filepath, { encoding: 'base64' });

    var imgDim = sizeOf(filepath)

    var entry = "." + styleClassName + "{";

    if (query.applyDimension != undefined && query.applyDimension === "true")
      entry += 
        "width: " + imgDim.width + "px;" + 
        "height: " + imgDim.height + "px;";

    if (applyStyle != undefined)
      for(styleKey in applyStyle)
        entry += styleKey + ":" + applyStyle[styleKey] + ";";

    entry += 
        "background-position: center;" + 
        "background-repeat: no-repeat;" +
        "background-image: url(data:image/" + extension + ";base64," + base64encodedImage + ');' +    //R0lGODlhUAAPANUAAAAAAAoKChQUFBoaGiQkJCwsLD4+PkJCQk1NTVNTU15eXmJiYmxsbHJycnt7e4ODg4iIiJZsbJqamqKioqqqqqxERLS0tL+/v8PDw8zMzNTU1Nvb2+Tk5O3t7fPz8/7+/v8AAP8ICP8WFv8cHP8jI/8yMv86Ov9CQv9KSv9VVf9lZf9ycv97e/+Dg/+Jif+Wlv+bm/+rq/+ysv+5uf/IyP/Q0P/f3//j4//19QAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAkAADkALAAAAABQAA8AAAb/wENjSCwaj8ikcskcVpSHx2dKrVqv2Kx2y52eZtmHtEsum8klEg4rnnI8007nI6/PPXDOnD7XxzlngVQkICxUNjA1H20cAApTBQMZAJSVGAMFHgAIUwYBHwMBcKFwgmciICAvKoQpU4yUCx2RkwQItwgamJqOHZ4fDQAWkwmmZjgzqcogJWuLUo2UAgGSAA4dF9keu5UB0x8YAAwPABTGXDQtJyHLqSI3VLAJDpTVDhaW3AgPAQCfoJREndsSA0U7ZTGqwFrwYYMAexscHABwKROAYhsIfOoAYAABABsGcrnBYkQ7V/GgAWD4gZa1VxS5FfvwSwIACBMAjBG5hcZBcBkpPzRi6bISpYqaZv4yAEAPAAI8uSQDYaKFyRHO2viCMGVBgg0GworVkGCBBwMOpjBA4OEASwYGAEXNEgPECHgfZKBY8Wrn3L9cYICgYQXvA66AE2txAUMLhAMQIkuWLKay5cuYM2vezNlyBM4HggAAOw==);" // + "" + //url('~!!url?limit=1024!#{file}');
        "}";

    styleClassChunks.push(entry);
  }
  
  return  styleClassChunks.join('\n');
}

