/// <reference path="../../../../typings/tsd.d.ts" />

import {Logger} from '../../../log/Logger';
import {AppLogger} from '../../../log/AppLogger';


require('!!style?singleton!raw!./loaders/images-as-css-loader!./categoryNameToImagePathMappings.json')

interface Map {
	[index: string]: string
}

// TODO: extract currency info to separate service
export class UnitImageService {
    protected log: Logger = new AppLogger(this);

	constructor() {
		this.log.v('construct');	
	}

	getCategoryImageClass(categoryName: string): string {
		return 'category-' + categoryName.toLowerCase().replace(' ', '-');
	}

/*
	getCurrencyName(currencyCode: string): string {
		var currency: Currency = currencyMap[currencyCode];
		if (currency == undefined)
			return undefined;

		return currency.name;
	}

	getCountryImageClassByCurrencyCode(currencyCode: string): string {
		var currency: Currency = currencyMap[currencyCode];
		var doesMappingExist = (currency != undefined && currency.countries.length > 0 && currency.countries[0].code != undefined);
		
		var countryCode: string = (doesMappingExist === true)? currency.countries[0].code : currencyCode.substr(0,2);
			
		if (doesMappingExist === false)	
			this.log.v('no entry found for currency code \'{0}\', defaulting to \'{1}\'', currencyCode, countryCode);

		return this.getCountryImageClass(countryCode);
	}*/
}