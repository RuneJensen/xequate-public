/// <reference path="../../../typings/tsd.d.ts" />

import {Logger, AppLogger} from '../../xequate/log';
import {MathExpert} from '../../xequate/util';


export interface Focusable {
	setFocus(): void;
}

export class FocusLoopService {
    protected log: Logger = new AppLogger(this); 

	private focusables : Array<Focusable> = [];

	constructor() {
		this.log.v('construct');
	}

	addFocusable(focusable: Focusable) {
		this.log.d('adding focusable');

		this.focusables.push(focusable);
	}

	focusNext(focusable: Focusable): void {
		this.log.d('focusing next');

		var nextIndex: number = this.focusables.indexOf(focusable) +1;
		nextIndex = MathExpert.mod(nextIndex, this.focusables.length); // enforce circular indexes

		this.focusables[nextIndex].setFocus();
	}

	focusPrevious(focusable: Focusable): void {
		this.log.d('focusing previous');

		var previousIndex: number = this.focusables.indexOf(focusable) -1;
		previousIndex = MathExpert.mod(previousIndex, this.focusables.length); // enforce circular indexes

		this.focusables[previousIndex].setFocus();
	}

}