import {Logger} from '../../log/Logger';
import {AppLogger} from '../../log/AppLogger';
import {LocalStorageDataGateway} from '../../xequate/data';


// For DI.
export class AppDataGateway extends LocalStorageDataGateway {
	constructor() {
		super();
	}
}