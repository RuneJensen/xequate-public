/// <reference path="../../typings/tsd.d.ts" />

import {Component, View} from 'angular2/angular2';
//import {Inject, InjectPromise, Injectable} from 'angular2/src/di/decorators';
import {Home} from './components/home/home';
//import {Promise, PromiseWrapper} from 'angular2/src/facade/async';
import {Logger, AppLogger} from '../xequate/log';

@Component({
    selector: 'app',
})
@View({
	template: '<home></home>',
    directives: [Home]
    //styleUrls: ['http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css']	
})
export class App {
    private log: Logger = new AppLogger(this); 

    //myForm: ControlGroup;
    

	//constructor(@InjectPromise(TodoStore) todoStorePromise:Promise<TodoStore>) {
    constructor() {
		const buildType = CONFIGURATION.BUILD_TYPE;
		const majorVersion = CONFIGURATION.BUILD_MAJOR_VERSION;
		const minorVersion = CONFIGURATION.BUILD_MINOR_VERSION;

		this.log.d('construct');
		this.log.i('build is \'{0}\' ({1}.{2})', buildType, majorVersion, minorVersion);
	}

}


