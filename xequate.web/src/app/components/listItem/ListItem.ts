/// <reference path="../../../../typings/tsd.d.ts" />
import {Ancestor, Attribute, Component, View, bootstrap, NgFor, bind, EventEmitter, ElementRef} from 'angular2/angular2';
import {DropdownInput} from '../dropdownInput/DropdownInput';
import {Logger, AppLogger} from '../../../xequate/log';


@Component({
    selector: 'list-item',
    //properties: { 'test': 'test' },
    //hostProperties: { sortKey: 'sortKey' },
    events: ['init', 'click']
})
@View({
    template: require('./listItem.html'),
    //templateUrl: 'listItem.html',
    directives: [NgFor],
    //styleUrls: ['list-item.css'] 
    styles: [require('./list-item.scss')] 
})
export class ListItem {
    private log: Logger = new AppLogger(this);

    init: EventEmitter = new EventEmitter();
    click: EventEmitter = new EventEmitter();

    isSelected: boolean = false;
    isVisible: boolean = true;
    //sortKey: string;
    
    sortIndex: number;
    value: any; 

    constructor(
        @Ancestor() private container: DropdownInput,
        private elementRef: ElementRef
        )
    {
        //console.log(sortIndex);
        //this.sortIndex = parseInt(sortIndex);

        container.listItemCreated(this);  
    }

    compareTo(item: ListItem): number {   // TODO: implement interface for comparable 
        if (this.sortIndex === undefined)
            throw 'Cannot compare as sort index has not been defined on this list item';
        if (!item.sortIndex === undefined)
            throw 'Cannot compare as sort index has not been defined on the list item to compare with';

        return this.compareTo(item);
    }

    scrollIntoView(alignTop: boolean): void {
        var el = this.elementRef.nativeElement;

        var rect = el.getBoundingClientRect();
        var itemHeight = rect.bottom - rect.top;
        var verticalBuffer = itemHeight * (1/2.5);

        var isOutsideWindowTop = (rect.top < verticalBuffer);
        var isOutsideWindowBottom = (rect.bottom > window.innerHeight + verticalBuffer);

        if (isOutsideWindowTop === true)
            window.scroll(window.scrollX, window.scrollY + rect.top - verticalBuffer);

        if (isOutsideWindowBottom === true)
            window.scroll(window.scrollX, window.scrollY + (rect.bottom - window.innerHeight + verticalBuffer));

        //el.scrollIntoView(isOutsideWindowTop);  
    }

    onInit() {
        this.init.next(this);
    }
     
    onDestroy() {
        this.container.listItemDestroyed(this);  
    }

    onClick(event) {
        this.click.next(this);
    }
}




