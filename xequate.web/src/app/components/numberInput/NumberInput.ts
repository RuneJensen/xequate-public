/// <reference path="../../../../typings/tsd.d.ts" />

import {Attribute, Component, View, bootstrap, NgFor, NgIf, bind, EventEmitter, ElementRef} from 'angular2/angular2';
import {Logger} from '../../../log/Logger';
import {AppLogger} from '../../../log/AppLogger';
import {Editable} from '../editable/Editable';
import {InvalidEvaluator} from '../../../xequate/component';


@Component({
    selector: 'number-input',
    events: ['valuechange', 'blur', 'focus'],
    //lifecycle: ['init']
    properties: [
        'value', /*'numberPattern',*/ 
        'isautofocusenabled'
        ]
})
@View({
    //templateUrl: 'numberInput.html',
    template: require('./numberInput.html'),
    directives: [Editable]
})
export class NumberInput {
    protected log: Logger = new AppLogger(this); 
   
    private valuechange: EventEmitter = new EventEmitter();
    private blur: EventEmitter = new EventEmitter();
    private focus: EventEmitter = new EventEmitter();
   
    private _textValue = '';
    private _value = 0;
    private numberPattern = /^\d*([\.,]\d*)?$/;
    private isInvalid = false;
    
    emptyValue = 0;
    isAutoFocusEnabled = false;


    constructor(private elementRef: ElementRef) {
        this.log.v('construct');
    }

    onValueChange(event: Event, element: Editable) {
        this.textValue = element.value;

        this.isInvalid = element.isInvalid; 
    }    

    areValuesEqual(s1: string, s2: string): boolean {  // note: function will be passed in binding without context
        s1 = s1.toString().replace(',', '.');
        s2 = s2.toString().replace(',', '.');

        return parseFloat(s1) === parseFloat(s2);
    }

    private onEditableFocus(event: FocusEvent) {
        this.focus.next(event);
    }

    private commaToDot(str: string) {
        return str.replace(',', '.');
    }

    private onEditableBlur(event) {
        this.blur.next(event);
    }

    /*private get editableElement(): any {
        return this.elementRef.nativeElement.querySelector('editable');
    }*/

    set value(value: number) {
        if (this._value === value)
            return;

        this._value = value;
        this.valuechange.next(new Event('valuechange'));

        this.textValue = value + '';
    }

    get invalidEvaluator(): InvalidEvaluator {
        return (value: string): boolean => (value !== '' && this.numberPattern.test(value) === false);
    } 

    get textValue(): string {
        return this._textValue;
    }

    set textValue(value: string) {
        if (this._textValue === value)
            return;

        this._textValue = value;

        var newValue: number = parseFloat(this.commaToDot(this.textValue));
        var newValueIsNumber = !isNaN(newValue);

        if (newValueIsNumber === true && newValue !== this.value) {
            this.value = newValue;
        }
    }

    // Binding properties

    // get isinvalid(): boolean {
    //     return true; //this.editableElement.isinvalid;
    // }
    
    set isautofocusenabled(value: boolean) { this.isAutoFocusEnabled = value; }

    get value(): number { return this._value; }
}
