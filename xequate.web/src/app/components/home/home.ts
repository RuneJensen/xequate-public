/// <reference path="../../../../typings/tsd.d.ts" />

import {ViewContainerRef, Directive, Component, View, bootstrap, NgFor, bind, NgIf, ElementRef, EventEmitter} from 'angular2/angular2';
//import {If} from 'angular2/di';

//import {InjectPromise} from 'angular2/angular2';
//import {Inject, InjectPromise, Injectable} from 'angular2/src/di/decorators';

import {CodeInput} from '../codeInput/CodeInput'
import {Editable} from '../editable/Editable';
import {NumberInput} from '../numberInput/NumberInput';
import {DropdownInput} from '../dropdownInput/DropdownInput';
import {ListItem} from '../listItem/ListItem';
//import {AngularFire, FirebaseArray} from 'firebase/angularfire';

import {Logger, AppLogger} from '../../../xequate/log';
import {DataGatewayState, CategoryProvider, Category, CategoryItem, EquationState} from '../../../xequate/data';
import {AppDataGateway, AppCategoryProvider} from '../../../xequate/service';
import {CaretExpert, CharCodeExpert, CaretInfo, MathExpert, StringExpert} from '../../../xequate/util';
import {ToolTip} from '../../decorators/ToolTip'

//import {FormBuilder, ControlGroup, formDirectives, Validators} from 'angular2/forms';
//import {FormBuilder, Validators, formDirectives, ControlGroup} from 'EditingSide.Right';
//import {Promise, PromiseWrapper} from 'angular2/src/facade/async';


const DEFAULT_LEFT_CODE  = CONFIGURATION.DEFAULT_LEFT_CODE,
      DEFAULT_RIGHT_CODE = CONFIGURATION.DEFAULT_RIGHT_CODE;


enum EditingSide {
    Left,
    Right
}


@Directive({
    selector: '[tooltip-error]',
    properties: [ 'errorMessage: tooltip-error' ]                         
})
export class ErrorMessage {
    constructor(private viewContainer: ViewContainerRef) { }

    set errorMessage(value: string) { 
        if (value != undefined && value !== '')
            this.viewContainer.element.nativeElement.setAttribute('tooltip-error', value);
        else
            this.viewContainer.element.nativeElement.removeAttribute('tooltip-error');
    }
}

@Component({
    selector: 'home',
    properties: [   // Angular uses property bindings to automatically sync up the component tree with the model, and the DOM with the component tree. If I had to find all the places that might depend on the new value and manually update them, that would be tedious and error-prone. I want the application to reflect this change automatically. That is what property bindings are for: at the end of the VM turn, Angular will check every component in the component tree. More specifically, it will check every property binding (every square bracket, every pair of curly braces), and will update the components. It will also update the DOM to match the state of the component tree.
        //'value': 'value'
        //'categoryitemcategorymismatcherrormessage'
    ],
	appInjector: [
        //AppDataGateway,
        //FormBuilder,
        //bind(Logger).toFactory(() => { return new ConsoleLogger() }),  // returns new instance on each injection request, but the framework appears broken here
        //bind(Logger).toClass(ConsoleLogger),  
        //bind(Serializer).toClass(JsonSerializer), 
        //DataGateway,
        
        //bind(DataGateway).toClass(FirebaseGateway), // bind the data gateway interface to a singleton service
		// AppDataGateway,

        //bind(TodoStore).toValue(new TodoStore())
		/*
		bind(TodoStore).toAsyncFactory(() => {
			console.log('aaaaaaaa');
			return new Promise((resolve) => {

				console.log('hmmm 1..');
				var FIREBASE_URL:string = 'https://fiery-heat-5927.firebaseio.com/rates';
		    	ratesStore = new Firebase(FIREBASE_URL);
				ratesStore.orderByKey().limitToLast(1).on('child_added', (latestDataHolder) => {
					var data:Object = latestDataHolder.val();
					resolve(new TodoStore(data));
				});

				console.log('hmmm 2..');

		 	});
		})*/
        ]
})
@View({
	//templateUrl: 'home.html',
    template: require('./home.html'),
    directives: [
        ListItem, NgFor, NgIf, 
        CodeInput, Editable, NumberInput, ErrorMessage //, 
        ,ToolTip
        //DropdownInput
        //, formDirectives
    ],
    styles: [require('./home.scss')]
    // styleUrls: ['home.css']
})
export class Home {
    private log: Logger = new AppLogger(this); 

    //private categories: 
    private categoryProvider: CategoryProvider;  

    //myForm: ControlGroup;
    
    private equationState: EquationState;

    private formHasBeenAutoFocused = false;

    private _editingSide: EditingSide = EditingSide.Left;

    private mediaQueryEvent = new EventEmitter(); 


    private get editingSide(): EditingSide {
        return this._editingSide;
    }

    private set editingSide(value: EditingSide) {
        if (this._editingSide === value) 
            return;

        this._editingSide = value; 

        this.log.d('Editing side changed to: ' + value);
    }

    canConvert = false;

    private mediaMatchSMallWidth;

    private updateEquationStateTimeout: any;
    private updateEquationStateDelayMilliSeconds = 500;

    //constructor(@InjectPromise(TodoStore) todoStorePromise:Promise<TodoStore>) {
    constructor(
        private elementRef: ElementRef,
        private dataGateway: AppDataGateway,
        categoryProvider: AppCategoryProvider
    ) {
        this.log.v('construct');

        this.categoryProvider = categoryProvider;

        this.categoryProvider.currencyDataUpdated.toRx().subscribe(() => this.handleDataAvailable());
        if (this.categoryProvider.isCurrencyDataAvailable === true)
            this.handleDataAvailable();

        /*
        this.myForm.valueChanges.subscribe(function (value) {
            console.log(value);
        }.bind(this));
        */

        this.mediaMatchSMallWidth = window.matchMedia("(max-width: 768px)");
        //console.log(mq);
        this.mediaMatchSMallWidth.addListener((new_mq) => {
            this.mediaMatchSMallWidth = new_mq;
        });
    }

    persistEquationStore(): void {
        clearTimeout(this.updateEquationStateTimeout);
        this.updateEquationStateTimeout = setTimeout(
            () => this.dataGateway.equationState = this.equationState,
            this.updateEquationStateDelayMilliSeconds
            );
    }

    getDefaultEquationState(): EquationState {
        return {
            leftValue: 1,
            rightValue: 1,
            leftCategoryItem: this.categoryProvider.getFirstCategoryByKey(DEFAULT_LEFT_CODE),
            rightCategoryItem: this.categoryProvider.getFirstCategoryByKey(DEFAULT_RIGHT_CODE)
        };
    }

    // Callbacks

    handleDataAvailable(): void {
        this.log.d('home component notified of currency data update');

        this.equationState = this.dataGateway.equationState;
        if (this.equationState == undefined)
            this.equationState = this.getDefaultEquationState();
        
        this.canConvert = true;
        this.convertNonEditingSide()
    }

    areItemsInSameCategory(item1: CategoryItem, item2: CategoryItem): boolean {
        return item1.key === item2.key;
    }

    areCodesEqual(code1: string, code2: string) {
        return code1.toUpperCase() === code2.toUpperCase();
    }

    onLeftCodeChange(leftCodeInput: CodeInput) {
        if (this.equationState != undefined && 
            false === this.areItemsInSameCategory(this.equationState.leftCategoryItem, leftCodeInput.categoryItem)
            ) {
            this.equationState.leftCategoryItem = leftCodeInput.categoryItem;
            this.convertNonEditingSide();       
            this.persistEquationStore();     
        }
    }

    onRightCodeChange(rightCodeInput: CodeInput) {
        if (this.equationState != undefined && 
            false === this.areItemsInSameCategory(this.equationState.rightCategoryItem, rightCodeInput.categoryItem)
            ) {
            this.equationState.rightCategoryItem = rightCodeInput.categoryItem;
            this.convertNonEditingSide();
            this.persistEquationStore();     
        }    
    }

    onLeftValueChange(leftNumberInput: NumberInput) {
        if (this.equationState != undefined && this.equationState.leftValue !== leftNumberInput.value) {
            this.equationState.leftValue = leftNumberInput.value;
            this.convertNonEditingSide();            
            this.persistEquationStore();     
        }
    }

    onRightValueChange(rightNumberInput: NumberInput) {
        if (this.equationState != undefined && this.equationState.rightValue !== rightNumberInput.value) {
            this.equationState.rightValue = rightNumberInput.value;
            this.convertNonEditingSide();            
            this.persistEquationStore();     
        }
    }

    convert(fromKey: string, fromValue: number, toKey: string, category: Category): number {
        var baseKey = category.baseKey;
        var fromRate: number = category.map[fromKey];
        var fromValueInBase = fromValue / fromRate;
        
        var toRate: number = category.map[toKey];
        var toValue: number = fromValueInBase * toRate;
        toValue = MathExpert.round(toValue, 4);

        this.log.d('Converted {0} {1} ({2} {3}) to {4} {5}', fromValue, fromKey, fromValueInBase, baseKey, toValue, toKey);

        return toValue;
    }

    convertNonEditingSide(): void {
        if (this.canConvert === false) {
            this.log.d('Conversion ignored, cannot convert yet');
            return;
        }

        if (this.equationState.leftCategoryItem.category.name !== this.equationState.rightCategoryItem.category.name) {
            this.log.e('Conversion ignored as there is a category mismatch');
            return;
        }

        // if (this.equationState.leftCategoryItem.key === this.equationState.leftCategoryItem.key) {
        //     this.log.e('Conversion ignored as the category items are the same');
        //     return;
        // }

        var category: Category = this.equationState.leftCategoryItem.category;

        if (this.editingSide === EditingSide.Left)
            this.equationState.rightValue = this.convert(
                this.equationState.leftCategoryItem.key,
                this.equationState.leftValue,
                this.equationState.rightCategoryItem.key, 
                category);
 
        if (this.editingSide === EditingSide.Right)
            this.equationState.leftValue = this.convert(
                this.equationState.rightCategoryItem.key,
                this.equationState.rightValue,
                this.equationState.leftCategoryItem.key, 
                category);
    }

    private maybeFocusNestedAutoFocusElement() { // TODO: abstract, e.g. via dedicated service or directive
        requestAnimationFrame(() => {
            var focusAttrName = 'contenteditable'; // TODO: this is a workaround, implement isautofocusenabled attribute properly
            var target: HTMLElement = this.elementRef.nativeElement.querySelector(`[${focusAttrName}]`);
            if (target != undefined) 
                target.focus()
        });
    }

    private onLeftInputFocus() {
        this.editingSide = EditingSide.Left;
    }

    private onRightInputFocus() {
        this.editingSide = EditingSide.Right;
    }

    get isRequestingDataAndIsWithoutCache(): boolean {
        return this.categoryProvider.state.contains(DataGatewayState.IsUpdatingData) && !this.categoryProvider.state.contains(DataGatewayState.HasCachedData);
    }

    get isRequestingDataAndRunningWithCache(): boolean {
        return this.categoryProvider.state.contains(DataGatewayState.IsUpdatingData) && this.categoryProvider.state.contains(DataGatewayState.HasCachedData);
    }

    get isRequestFailed(): boolean {
        return this.categoryProvider.state.contains(DataGatewayState.DataFetchFailed);
    }

    get canPerformConversions(): boolean {
        var result = (this.canConvert === true); // (this.categoryProvider.isCurrencyDataAvailable === true);
        if (result === true && this.formHasBeenAutoFocused === false) {
            this.formHasBeenAutoFocused = true;
            this.maybeFocusNestedAutoFocusElement();
        }

        return result;
    }

    get areCategoryItemCategoriesInMismatch(): boolean {
        if (!this.areBothCategoryItemsSpecified)
            return false;

        return this.equationState.leftCategoryItem.category.name != this.equationState.rightCategoryItem.category.name;
    }

    get categoryItemCategoryMismatchErrorMessage(): string {
        if (!this.areBothCategoryItemsSpecified)
            return '';

        var fromItemKey = this.equationState.leftCategoryItem.key;
        var fromCategoryName = this.equationState.leftCategoryItem.category.name;

        var toItemKey = this.equationState.rightCategoryItem.key;
        var toCategoryName = this.equationState.rightCategoryItem.category.name;

        return StringExpert.format(
            'Cannot equate {0} ({1}) and {2} ({3})',
            fromItemKey,
            fromCategoryName.toLowerCase(),
            toItemKey,
            toCategoryName.toLowerCase()
            );
    }

    get areCategoriesInMismatch(): boolean {
        return false === (
            this.areBothCategoryItemsSpecified === true && 
            this.equationState.leftCategoryItem.category.name === this.equationState.rightCategoryItem.category.name
            );
    }

    get areBothCategoryItemsSpecified(): boolean {
        return this.equationState != undefined && 
               this.equationState.leftCategoryItem != undefined && 
               this.equationState.rightCategoryItem != undefined;
    }

    get isDataUpdated(): boolean {
        return this.categoryProvider.state.contains(DataGatewayState.HasUpdatedData);
    }

    get shouldDisplayLeftSideInvalid(): boolean {
        return this.editingSide == EditingSide.Right && 
               this.areCategoryItemCategoriesInMismatch;
    }

    get shouldDisplayRightSideInvalid(): boolean {
        return this.editingSide == EditingSide.Left && 
               this.areCategoryItemCategoriesInMismatch;
    }

    get isWidthSmall(): boolean {
        return this.mediaMatchSMallWidth.matches === true;
    }

    get categoryitemcategorymismatcherrormessage() { return this.categoryItemCategoryMismatchErrorMessage }


    get leftValue(): number {
        return this.equationState != undefined? this.equationState.leftValue: 0;
    }

    get rightValue(): number {
        return this.equationState != undefined? this.equationState.rightValue: 0;
    }

    get leftCategoryItem(): CategoryItem {
        return this.equationState != undefined? this.equationState.leftCategoryItem: undefined;
    }

    get rightCategoryItem(): CategoryItem {
        return this.equationState != undefined? this.equationState.rightCategoryItem: undefined;
    }

}






