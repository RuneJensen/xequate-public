/// <reference path="../../../../typings/tsd.d.ts" />

import {CSSClass, Attribute, Component, View, bootstrap, NgFor, NgIf, bind, EventEmitter, ElementRef} from 'angular2/angular2';
import {InvalidEvaluator, DisplayCategoryItem} from '../../../xequate/component';
import {DropdownInput} from '../dropdownInput/DropdownInput';
import {ListItem} from '../listItem/ListItem';

import {Logger, AppLogger} from '../../../xequate/log';
import {CategoryProvider, Category, CategoryItem} from '../../../xequate/data';
import {AppCategoryProvider, CurrencyImageService, UnitImageService} from '../../../xequate/service';
import {ValueChangeEvent} from '../../../xequate/event';
import {StringExpert, CharCodeExpert, KeyMap} from '../../../xequate/util';


@Component({
    selector: 'code-input',
    properties: ['value', 'categoryitem'],
    events: ['valuechange', 'validcodechange', 'focus'],
})
@View({
	//templateUrl: 'codeInput.html',
    template: require('./codeInput.html'),
    //template: `this is a code input`,
    directives: [ListItem, NgFor, NgIf, DropdownInput, CSSClass],
    styles: [require('./code-input.scss')],
    //styleUrls: ['http://fonts.googleapis.com/css?family=Lucida+Console']
})
export class CodeInput {
    protected log: Logger = new AppLogger(this); 
    private categoryProvider: CategoryProvider;  
    
    valuechange = new EventEmitter();
    validcodechange = new EventEmitter();
    focus = new EventEmitter();

    private _value = '';
    private _categoryItem: CategoryItem;

    get defaultValue(): string {
        if (this.categoryItem == undefined)
            return '';

        return this.categoryItem.key;
    }

    // defaultFromCurrencies = ['USD', 'GBP', 'EUR']; 
    // defaultToCurrencies = ['EUR', 'GBP', 'USD']; 

    keyMap: KeyMap;
    blockKeyCodes: Array<number>; 

    constructor(
        private currencyImageService: CurrencyImageService,
        private unitImageService: UnitImageService,
        categoryProvider: AppCategoryProvider
    	) {
        this.log.v('construct');

        this.categoryProvider = categoryProvider; 

        //this.keyMap[CharCodeExpert.Codes.ENTER.toString()] = CharCodeExpert.Codes.TAB;
        this.blockKeyCodes = [
            CharCodeExpert.Codes.ENTER,
            CharCodeExpert.Codes.DOWN, // Firefox uses up/down to blur widget on caret edge positions
            CharCodeExpert.Codes.UP
            ];
    }

    onValueChange(event, element: DropdownInput) {
        this.value = element.value;
     }

    private isCurrency(category) {
        return category.name === "Currency";  
    } 

    getDisplayCategoryItems(): Array<DisplayCategoryItem> {
        var filter: string = this.value.toUpperCase();
        var isFilterEmpty = (filter === '')

        var keys = this.categoryProvider.categoryKeys;

        keys.forEach((item: any, index: number) => { // TODO: exploit js structural interface matching to extend with interface for a 'display rate'
            item.sortIndex = index;
            var isItemAlreadyExtendedToDisplayItem = (item.primaryName != undefined);
            if (isItemAlreadyExtendedToDisplayItem === false) {
                if (this.isCurrency(item.category) === true) { // handle the special case of currency
                    var currencyCode: string = item.key;
                    var currencyName = this.currencyImageService.getCurrencyName(currencyCode)
                    
                    item.primaryName = (currencyName != undefined)? currencyName : item.key;
                    item.secondaryName = currencyCode;
                }
                else {
                    item.primaryName = item.key;
                    item.secondaryName = item.category.name;
                }
            }

            var isCategoryItemSelected = this.categoryItem != undefined && 
                                         this.categoryItem.category.name === item.category.name &&
                                         this.value === item.key;

            item.isVisible = (isCategoryItemSelected === false) &&
                             (isFilterEmpty? true : this.isCodeToBeDisplayed([item.primaryName, item.secondaryName], filter));
       });


        return keys;
    }

    isCodeToBeDisplayed(searchStrings: Array<string>, filter: string): boolean {
        if (filter.length === 0)
            return true;

        for(var searchString of searchStrings) 
            if (searchString != undefined && searchString.toUpperCase().indexOf(filter) >= 0)
                return true;

        return false;
    }

    onListItemInit(listItem: ListItem, categoryDisplayItem: any, flag: HTMLElement) { 
        listItem.sortIndex = categoryDisplayItem.sortIndex; //parseInt(rate.sortIndex);
        listItem.value = categoryDisplayItem.key;

        // Set the flag image class, not really proper Angular2 but we need this hack for speed
        
        var tmpKey = 'currentFlagClassName'; // wokaround: angular appears to recycle the item, causing multiple (and wrong) past classes to be present
        flag.classList.remove(flag[tmpKey]);

        // var isCurrency = this.isCurrency(categoryDisplayItem.category);

        var className: string;
        switch (categoryDisplayItem.category.name) {
            case "Currency":
                var currencyCode = categoryDisplayItem.key;
                className = this.currencyImageService.getCountryImageClassByCurrencyCode(currencyCode);
                if (className !== '')
                    break;            
            default:
                var categoryName = categoryDisplayItem.category.name;
                className = this.unitImageService.getCategoryImageClass(categoryName);
        }

        flag[tmpKey] = className;
        flag.classList.add(className);
    }

    private isInvalid(str: string): boolean {
        if (str.length === 0)
            return false; 

        return !this.categoryProvider.doesCategoryKeyExist(str);
    }

    private onDropdownFocus(event: FocusEvent) {
        this.focus.next(event);
    }

    onDropdownListInit(input: DropdownInput): void {  // bug workaround: replace with property binding when possible
        input.invalidEvaluator = (str: string) => this.isInvalid(str); //code => code.length > 3 || (code.length === 3 && !this.currencyProvider.doesCodeExist(code)); 
    }

    onListItemMouseDown(categoryItem) {
        this.log.d('item clicked: ' + JSON.stringify(categoryItem));
    }

    get value(): string {
        return this._value;
    }

    set value(value: string) {
        if (value == undefined) {
            this.log.e('ignoring undefined value');
            return;
        }

        if (this._value === value) {
            this.log.d('ignoring redundant value');
            return;
        }

        var oldValue = this._value;

        this._value = value;
        this.log.v('value change, "{0}" !== "{1}"', oldValue, value);    
        
        this.valuechange.next(new Event('valuechange'));


        this.maybeSetSelectedCategoryItemByValue();
    } 

    private maybeSetSelectedCategoryItemByValue(): void {
        var isValueEqualToSelectedCategoryItemKey: boolean = this.categoryItem != undefined &&  
                                                             this.categoryItem.key === this.value;
        if (isValueEqualToSelectedCategoryItemKey === false) {
            var categoryItem: any = this.categoryProvider.getFirstCategoryByKey(this.value); 
            if (categoryItem != undefined) 
                this.categoryItem = categoryItem;
        }
    }

    get categoryItem(): CategoryItem { 
        return this._categoryItem; 
    } 

    set categoryItem(value: CategoryItem) {
        if (value == undefined) {
            this.log.e('ignoring undefined category item');
            return;
        }

        this.log.v('category item change from {0} to {1}', this._categoryItem, value);
        this._categoryItem = value;

        this.validcodechange.next(new Event('validcodechange'));

        this.value = value.key;
    }

    get categoryitem(): CategoryItem {
        return this.categoryItem;
    }

    set categoryitem(value: CategoryItem) {
        this.categoryItem = value;
    }

}


