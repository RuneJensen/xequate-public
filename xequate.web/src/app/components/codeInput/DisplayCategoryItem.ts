import {CategoryItem} from '../../../xequate/data';


export interface DisplayCategoryItem extends CategoryItem {
    name: string;
    isVisible: boolean;
    sortIndex: number;
    category: any;
}

