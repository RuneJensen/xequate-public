/// <reference path="../../../../typings/tsd.d.ts" />

import {Attribute, Component, View, bootstrap, NgFor, NgIf, bind, EventEmitter, ElementRef} from 'angular2/angular2';
import {Logger, AppLogger} from '../../../xequate/log';
import {CaretExpert, CaretInfo, CharCodeExpert, DomExpert, StringExpert, TypeExpert, EventExpert, KeyMap} from '../../../xequate/util';
////import {IdGenerator} from '../../services/IdGenerator';
import {ValueChangeEvent} from '../../../xequate/event';
import {FocusLoopService, Focusable} from '../../../xequate/service';
import {EqualityEvaluator} from 'EqualityEvaluator';
import {InvalidEvaluator} from 'InvalidEvaluator';


@Component({
    selector: 'editable',
    events: [
        'init', 
        'keyup', 
        'keydown', 
        'keypress', 
        'focus', 
        'blur', 
        'input', 
        'valuechange',
        //'invalidevaluated'
        ],
    //lifecycle: ['init']
    properties: [
        'value', 
        'enforcedvaluepattern', 
        'defaultvalue', 
        'equalityevaluator',
        'isautofocusenabled', 
        'keymap', 
        'blockkeycodes', 
        'invalidevaluator',
        'isinvalid'
        ] 
})
@View({
    template: require('./editable.html'),
    styles: [require('./editable.scss')]    
})
export class Editable implements Focusable {
    protected log: Logger = new AppLogger(this); 
   

    private init: EventEmitter = new EventEmitter();
    private keyup: EventEmitter = new EventEmitter();
    private keydown: EventEmitter = new EventEmitter();
    private keypress: EventEmitter = new EventEmitter();
    private blur: EventEmitter = new EventEmitter();
    private focus: EventEmitter = new EventEmitter();
    private input: EventEmitter = new EventEmitter();
    private valuechange: EventEmitter = new EventEmitter();
    private invalidevaluated: EventEmitter = new EventEmitter();

    private enforcedValuePattern: RegExp;
    private inputFilterPattern: RegExp;
    
    private invalidInputClassName = 'editable-defaultInvalidInputAnimation'; // must correlate with value in stylesheet
    private invalidInputClassDurationSeconds = 0.2; // must correlate with value in stylesheet
   
    invalidEvaluator: InvalidEvaluator;

    private _value = '';

    focusNextCharCode: number = CharCodeExpert.Codes.TAB;

    equalityEvaluator: EqualityEvaluator = function(s1: string, s2: string): boolean { return s1 === s2; }
    set equalityevaluator(value: EqualityEvaluator) { this.equalityEvaluator = value; }

    isAutoFocusEnabled = false;
    
    blockKeyCodes: Array<number>;
    set blockkeycodes(value: Array<number>) { this.blockKeyCodes = value; }

    keyMap: KeyMap;
    set keymap(value: KeyMap) { this.keyMap = value; }

    defaultValue: string;
    set defaultvalue(value: string) { this.defaultValue = value; }

    constructor(
        private elementRef: ElementRef,
        private focusService: FocusLoopService,
        @Attribute('isAutoFocusEnabled') isAutoFocusEnabledStr: string,
        //@Attribute('defaultValue') public defaultValue: string,
        //@Attribute('value') public value: string,
        @Attribute('inputFilter') inputFilter: string,
        //@Attribute('enforcedValuePattern') enforcedValuePatternStr: string,
        @Attribute('invalidInputAnimationName') invalidInputClassName: string,
        @Attribute('invalidInputClassDurationSeconds') invalidInputClassDurationSecondsStr: string
        //public idGenerator: IdGenerator
        ) {
    	this.log.v('construct');

        focusService.addFocusable(this);

        if (invalidInputClassDurationSecondsStr != undefined)
            this.invalidInputClassDurationSeconds = parseFloat(invalidInputClassDurationSecondsStr); 

        if (invalidInputClassName != undefined)
            this.invalidInputClassName = invalidInputClassName;

        // if (enforcedValuePatternStr != undefined)
        //     this.enforcedValuePattern = new RegExp(enforcedValuePatternStr);

        if (inputFilter != undefined)
            this.inputFilterPattern = new RegExp(inputFilter);

        /*if (value != undefined)
            this.value = value;*/
        //setInterval(()=>{console.log(this.skod);}, 1000)

/*
        if (initialSizeStr != undefined) 
            this.initialSize = parseInt(initialSizeStr);

        if (minSizeStr != undefined) 
            this.minSize = parseInt(minSizeStr);

        if (isAutoSizeEnabledStr != undefined)
            this.isAutoSizeEnabled = Boolean(isAutoSizeEnabledStr);
        */
    }

    setFocus(): void {
        this.contentEditableElement.focus();
    }

    onInit() {
        this.init.next(this);
    }

    onInput(event: KeyboardEvent, element: HTMLSpanElement) {
        this.log.v('oninput');

        /*var valueChangeEvent: ValueChangeEvent<KeyboardEvent, string> = {
            event: event,
            value: element.textContent
        };*/

        this.value = element.textContent;

        this.input.next(event);
    }

    onInputKeyup(event: KeyboardEvent, element: HTMLSpanElement) {
        this.log.v('onkeyup');

        this.value = element.textContent;

        this.keyup.next(event);
    }

    onInputKeydown(event: KeyboardEvent, element: HTMLSpanElement) {
        this.log.v('onkeydown');

        var keyCode: number = event.which;

        if (this.keyMap != undefined && keyCode.toString() in this.keyMap) {
            var toCode = this.keyMap[keyCode.toString()];
            if (toCode != undefined) {
                this.log.d('mapping (blocked) key code from {0} to {1}', keyCode, toCode);

                EventExpert.simulateKeydown(toCode, element);
            }
            
            return false;
        }

        this.keydown.next(event);

        var isShiftModifierActive = (event.shiftKey === true);
        if (this.isKeyCodeToInvokeFocusNext(keyCode, isShiftModifierActive) === true) {
            this.focusService.focusNext(this);    
            return false;

        }
        if (this.isKeyCodeToInvokeFocusPrevious(keyCode, isShiftModifierActive) === true) {
            this.focusService.focusPrevious(this);
            return false;
        }

        if (this.isKeyCodeToBeBlocked(keyCode)) {
            this.log.d('blocking key code {0}', keyCode);
            return false;
        }
    }

    private isKeyCodeToInvokeFocusNext(keyCode: number, isShiftActive: boolean) {
        var caretInfo: CaretInfo = this.getCaretInfo();
        return (keyCode === this.focusNextCharCode && isShiftActive === false) ||
            (keyCode === CharCodeExpert.Codes.RIGHT && caretInfo.isLastPosition === true && caretInfo.isAllSelected === false);
    }

    private isKeyCodeToInvokeFocusPrevious(keyCode: number, isShiftActive: boolean) {
        return (keyCode === this.focusNextCharCode && isShiftActive === true) ||
               (keyCode === CharCodeExpert.Codes.LEFT && this.getCaretInfo().isFirstPosition === true);
    }

    private isKeyCodeToBeBlocked(keyCode: number): boolean {
        return this.blockKeyCodes != undefined && this.blockKeyCodes.indexOf(keyCode) >= 0;
    }

    onInputKeypress(event: KeyboardEvent, element: HTMLSpanElement): boolean {
        this.log.v('onkeypress');

        var inputCode = event.which;
        var inputChar: string = String.fromCharCode(inputCode);
        var resultingValue = element.textContent + inputChar;

        if (this.inputFilterPattern != undefined) {
            var isFilterPassed = this.inputFilterPattern.test(inputChar);
            if (isFilterPassed === false) {
                this.invokeBadInputAnimation(element);
                this.log.d('Input blocked, as "{0}" (char code {1}) did not pass input filter "{2}"', inputChar, inputCode, this.inputFilterPattern);
                
                return false;
            }
        }

        if (this.enforcedValuePattern != undefined) {
            var isFilterPassed = this.enforcedValuePattern.test(resultingValue);
            if (isFilterPassed === false) {
                this.invokeBadInputAnimation(element);
                this.log.d('Input blocked, as resulting value "{0}" did not pass value filter "{1}"', resultingValue, this.enforcedValuePattern);

                return false;
            }
        }

        this.keypress.next(event);

        return true;
    }

    invokeBadInputAnimation(element: HTMLElement): void {
        var animationClassName:   string = this.invalidInputClassName;
        var durationSeconds: number = this.invalidInputClassDurationSeconds;
        
        if (animationClassName != undefined && durationSeconds != undefined) {
            //element.style.animation = StringExpert.format('{0} {1}s', animationName, durationSeconds);
            element.classList.add(animationClassName);

            window.setTimeout(
                () => element.classList.remove(animationClassName), //element.style.animation = 'initial', 
                durationSeconds * 1000
                );
        }
    }

    onInputFocus(event: UIEvent, element: HTMLSpanElement) {
        this.log.v('onfocus');
 
        CaretExpert.selectContents(element);

        this.focus.next(event);
    }

    onInputBlur(event: UIEvent, element: HTMLSpanElement) {
        this.log.v('onblur');

        if (element.textContent.trim().length === 0 && this.defaultValue != undefined) {
            element.textContent = this.defaultValue; // bug-fix: should not be necessary due to binding (but apparently is)
            this.value = this.defaultValue;
        }

        this.blur.next(event);
    }

    selectAll() {
        CaretExpert.selectContents(this.contentEditableElement);
    }

    set enforcedvaluepattern(value: RegExp) {
        this.enforcedValuePattern = value;
    }

    get value(): string {
        return this._value.toString();
        //return this.elementRef.nativeElement.querySelector('span').textContent;
    }

    set value(value: string) {
        if (value == undefined) {
            this.log.e('ignoring undefined value');
            return;
        }

        value = value.toString();  // because consumers may pass any value type through the markup

        // if (this.equalityEvaluator != undefined && this.equalityEvaluator(this._value, value) === true)
        //     return;

        this.log.d('value change, "{0}" !== "{1}"', value, this._value);

        this._value = value;
        
        if (this.textContent !== value) { // hack to make things work with a contenteditable element
            var caretInfo: CaretInfo = this.getCaretInfo();
            this.textContent = value;

            if (caretInfo != undefined && caretInfo.isFocused === true && caretInfo.position > 0) {
                var newLastPosition = this.textContent.length;
                this.caretPosition = (newLastPosition < caretInfo.position)? newLastPosition : caretInfo.position;
            }
        }

        this.valuechange.next(new Event('valuechange'));
    }

    getCaretInfo(): CaretInfo {
        return CaretExpert.getCaretInfo(this.contentEditableElement);
    }

    set caretPosition(value: number) {
        CaretExpert.setCaretPosition(this.contentEditableElement.firstChild, value);
    }

    // Properties

    private get textContent(): string {
        return this.contentEditableElement.textContent;
    }

    private set textContent(value: string) {
        this.contentEditableElement.textContent = value;
    }

    private get contentEditableElement(): HTMLSpanElement {
        return this.elementRef.nativeElement.querySelector('span');
    }

    get isInvalid(): boolean {
        var result = (this.invalidEvaluator != undefined && this.invalidEvaluator(this.value) === true);
                
        return result;
    }


    // Binding properties

    set isautofocusenabled(value: boolean) {
        this.isAutoFocusEnabled = value;
    }

    get isinvalid(): boolean {
        return this.isInvalid;
    }

    set invalidevaluator(value: InvalidEvaluator) {
        this.invalidEvaluator = value;
    }
}

