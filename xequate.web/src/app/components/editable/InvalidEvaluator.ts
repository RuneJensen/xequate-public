export interface InvalidEvaluator { 
	(value: string): boolean; 
}
