export interface EqualityEvaluator {
    (s1: string, s2: string): boolean;
}

// interface DisplayRate extends Rate {
//     name: string;
//     isVisible: boolean;
//     sortIndex: number;
//     category: any;
// }
