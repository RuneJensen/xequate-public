/// <reference path="../../../../typings/tsd.d.ts" />

import {Attribute, Component, View, bootstrap, NgFor, bind, EventEmitter, ElementRef} from 'angular2/angular2';
import {ListItem} from '../listItem/ListItem';
import {Editable} from '../editable/Editable';
import {Logger, AppLogger} from '../../../xequate/log';
import {AppDataGateway} from '../../../xequate/service';
import {CaretExpert, MathExpert, CharCodeExpert, CaretInfo, KeyMap} from '../../../xequate/util';
import {ValueChangeEvent} from '../../../xequate/event';
import {InvalidEvaluator, } from '../../../xequate/component';

var topArrowBackgroundImageDataInject = require('url?limit=0!./popupArrow.png'); // will import raw as base64 encoded


@Component({
    selector: 'dropdown-input',
    properties: ['value', 'keymap', 'blockkeycodes', 'defaultvalue'],
    events: ['init', 'valuechange', 'keypress', 'keyup', 'keydown', 'focus'],
    //lifecycle: ['init']
})
@View({
    //templateUrl: 'dropdownInput.html',
    template: require('./dropdownInput.html'),
    directives: [Editable],
    //styleUrls: ['dropdown-input.css']    
    styles: [require('./dropdown-input.scss')]    
})
export class DropdownInput {
    protected log: Logger = new AppLogger(this); 

    private invalidInputClassName = 'dropdown-input-defaultInvalidValueClass';
    private static NO_SELECTION_INDEX = null; 
    private selectedIndex: number = DropdownInput.NO_SELECTION_INDEX;
    private items: Array<ListItem> = [];
    private maxLength: number;


    private visibilityPattern: RegExp;
    private _value: string = '';
    private forceHideUntilInput = false;

    invalidEvaluator: InvalidEvaluator;
    //isMenuVisible = false;
    get isMenuVisible(): boolean {
        var hasItems = (this.itemCount > 0);
        var isFailedByVisibilityPattern = () => this.visibilityPattern != undefined &&
                                                this.visibilityPattern.test(this.value) === false;

        return (this.forceHideUntilInput !== true) &&
               (this.isFocused === true) &&
               (hasItems === true) &&
               (isFailedByVisibilityPattern() === false);
    }

    private isFocused = false;

    keyMap: KeyMap; 
    set keymap(value: KeyMap) { this.keyMap = value; }

    init: EventEmitter = new EventEmitter();
    valuechange: EventEmitter = new EventEmitter();
    keypress: EventEmitter = new EventEmitter();
    keyup: EventEmitter = new EventEmitter();
    keydown: EventEmitter = new EventEmitter();
    change: EventEmitter = new EventEmitter();
    focus: EventEmitter = new EventEmitter();


    blockKeyCodes: Array<number>;
    set blockkeycodes(value: Array<number>) { this.blockKeyCodes = value; }

    topArrowBackgroundImageSource: string = topArrowBackgroundImageDataInject;

    defaultValue: string;
    set defaultvalue(value: string) { this.defaultValue = value; }

    constructor(
        private elementRef: ElementRef,
        /* @InjectPromise(...) promise, */
        @Attribute('visibilityPattern') visibilityPattern: string,
        @Attribute('placeholder') public placeholder: string,
        @Attribute('maxLength') public maxLengthStr: string,
        @Attribute('invalidInputClassName') invalidInputClassName: string,        
        public data: AppDataGateway) 
    {  
        this.log.v('construct');

        if (invalidInputClassName != undefined)
            this.invalidInputClassName = invalidInputClassName;

        if (maxLengthStr != undefined)
            this.maxLength = parseInt(maxLengthStr);

        if(visibilityPattern != undefined) 
            this.visibilityPattern = new RegExp(visibilityPattern);
    }

    onInit() {
        this.init.next(this);
    }
    
    listItemCreated(item: ListItem) {
        this.items.push(item);

        item.click.toRx().subscribe(() => { // TODO: syntax likely to change in upcoming angular2 version
            this.value = item.value;
        }); 
    }

    listItemDestroyed(item: ListItem) {
        var itemIndex: number = this.items.indexOf(item);
        this.items.splice(itemIndex, 1);
    }

    private isValidIndex(index: number) {
        return index >= 0 && index < this.itemCount;
    }

    // Callbacks

    onInput(event: KeyboardEvent, element: Editable) {
         this.value = element.value;
    }

    onKeypress(event: KeyboardEvent, element: Editable): boolean { // is only invoked by printable characters
        this.value = element.value;

        this.keypress.next(event)

        //return this.maxLength == undefined || this.maxLength >= (element.value.length +1);
        return true;
    }

    onKeyup(event: KeyboardEvent) {
        this.keyup.next(event);

        // this.setMenuVisibilityByPatternAndItemCount();
    }

    private reflectValueChange(): void {
        this.resetDropDownItemSelection();
        //this.setMenuVisibilityByPatternAndItemCount();
    }

    // private setMenuVisibilityByPatternAndItemCount() {
        // var hasItems = this.itemCount > 0;
        // var isBlockedByVisibilityPattern = () => this.visibilityPattern != undefined && 
        //                                          !this.visibilityPattern.test(this.value);
        
        // this.isMenuVisible = (this.isFocused === true) &&
        //                      (hasItems === true) &&
        //                      (isBlockedByVisibilityPattern() === false);
    // }

    onEditableFocus(event: FocusEvent) {
        this.isFocused = true;

        this.focus.next(event);
    }

    onEditableBlur(event: KeyboardEvent) {
        setTimeout( // delayed action allows for clicking dropdown item in firefox
            () => {
                this.isFocused = false;
            },
            50
            );    

    }

    onKeydown(event: KeyboardEvent, element: Editable): boolean {
        this.forceHideUntilInput = false;

        var isNotBlockingInput = true;

        if (this.isMenuVisible) {
            this.items.sort((i1, i2) => i1.sortIndex > i2.sortIndex? 1 : -1);   

            var charCode = event.which;
            switch (charCode) {
                case CharCodeExpert.Codes.DOWN:
                    this.setSelectedIndex(this.isNoneSelected ? this.firstIndex : this.selectedIndex + 1);
                    this.selectedItem.scrollIntoView(false);
                    break;
                case CharCodeExpert.Codes.UP:
                    this.setSelectedIndex(this.isNoneSelected ? this.lastIndex : this.selectedIndex - 1);
                    this.selectedItem.scrollIntoView(false);
                    break;
                case CharCodeExpert.Codes.ENTER: 
                    if (this.isNoneSelected === false) {
                        //element.value = this.selectedItem.value;                        
                        this.value = this.selectedItem.value;
                        element.selectAll();
                        this.forceHideUntilInput = true;
                    }
                    break;
                case CharCodeExpert.Codes.ESCAPE:
                    this.forceHideUntilInput = true;
                    isNotBlockingInput = false;
                    break;
            }
        }

        this.keydown.next(event);

        return isNotBlockingInput;
    }

    private resetDropDownItemSelection(): void {
        this.items.forEach(item => item.isSelected = false);

        this.selectedIndex = DropdownInput.NO_SELECTION_INDEX;
    }
    
    get selectedItem(): ListItem {
        return this.isNoneSelected ? null : this.items[this.selectedIndex];
    }

    private get firstIndex(): number { return 0; }

    private get lastIndex(): number { return this.items.length -1; }

    private get isNoneSelected(): boolean {
        return this.selectedIndex === DropdownInput.NO_SELECTION_INDEX;
    }

    private get itemCount(): number {
        return this.items.length;
    }

    private setSelectedIndex(value: number) {
        this.resetDropDownItemSelection();

        if (value !== DropdownInput.NO_SELECTION_INDEX) {
            this.selectedIndex = MathExpert.mod(value, this.itemCount);

            var item = this.items[this.selectedIndex];
            item.isSelected = true;
        }
    }

    get value(): string { return this._value; }
    set value(value: string) {
        this._value = value

        this.reflectValueChange();

        this.valuechange.next(new Event('valuechange'));
    }


}



