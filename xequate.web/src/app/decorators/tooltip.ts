/// <reference path="../../../typings/tsd.d.ts" />

import {Inject, Directive, ViewContainerRef, ProtoViewRef, ViewRef, View, Injector} from 'angular2/angular2';  // /annotations';
//import {ToolTipBox} from '../components/toolTipBox/ToolTipBox';


// require('!!style?singleton!raw!sass!./tool-tip.scss');

require.ensure(
    ['!!style?singleton!raw!sass!./tool-tip.scss'],
    () => require('!!style?singleton!raw!sass!./tool-tip.scss')
    );


@Directive({
    selector: '[tooltip-error],[tooltip]',
    // selector: ['[tooltip-error]', '[tooltip]'],
	properties: [              
        'tooltipText: tooltip',
        'tooltipErrorText: tooltip-error'          
    ],                         
})          
export class ToolTip {                 
    constructor(private viewContainer: ViewContainerRef)     { //, private protoViewRef: ProtoViewRef) {
    }

    private setHostAttribute(name: string, value: any) {
        var element = this.viewContainer.element.nativeElement;

        if (value != undefined && value.length > 0)
            element.setAttribute(name, value);
        else
            element.removeAttribute(name);
    }
                                 
    set tooltipText(value: string) {
        this.setHostAttribute('tooltip', value);
    }

    set tooltipErrorText(value: string) {
        this.setHostAttribute('tooltip-error', value);
    }


}