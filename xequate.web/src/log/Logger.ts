export interface Logger {
    i(message: any, ...args: Array<any>): void;
    info(message: any, ...args: Array<any>): void;

    v(message: any, ...args: Array<any>): void;
    verbose(message: any, ...args: Array<any>): void;

    d(message: any, ...args: Array<any>): void;
    debug(message: any, ...args: Array<any>): void;

    e(message: any, ...args: Array<any>): void;
    error(message: any, ...args: Array<any>): void;

    f(message: any, ...args: Array<any>): void;
    fatal(message: any, ...args: Array<any>): void;

    setCaller(caller: Object);
    omitLevel: boolean;
}
//export class Logger { constructor() { throw 'Construct called on interface'; } } 
