import {Logger} from './Logger';
import {LogLevel} from './LogLevel';
import {StringExpert} from '../util/StringExpert';

export class ConsoleLogger implements Logger {
    private isConsoleOutputEnabled;
    private _callerName: string = '(unknown caller)';
    omitLevel: boolean = true;
    isStyleEnabled: boolean = true;
    levelColorMap: { [_: string]: string } = {};

    constructor(caller?: Object|string) {
        if (caller)
            this.setCaller(caller);

        this.isConsoleOutputEnabled = CONFIGURATION.getIsConsoleOutputEnabled();

        this.levelColorMap[LogLevel.Info] = 'green';
        this.levelColorMap[LogLevel.Verbose] = 'lightgrey';
        this.levelColorMap[LogLevel.Debug] = 'gray';
        this.levelColorMap[LogLevel.Fatal] = 'wine';
        this.levelColorMap[LogLevel.Error] = 'red';
    }

    i(message: any, ...args: Array<any>): void {
        this.info(message, ...args);
    }

    info(message: any, ...args: Array<any>): void {
        this.logFormattedLogMessage(message, LogLevel.Info, args);
    }

    v(message: any, ...args: Array<any>): void {
        this.verbose(message, ...args);
    }

    verbose(message: any, ...args: Array<any>): void {
        this.logFormattedLogMessage(message, LogLevel.Verbose, args);
    }

    d(message: any, ...args: Array<any>): void {
        this.debug(message, ...args);
    }

    debug(message: any, ...args: Array<any>): void {
        this.logFormattedLogMessage(message, LogLevel.Debug, args);
    }

    e(message: any, ...args: Array<any>): void {
        this.error(message, ...args);
    }

    error(message: any, ...args: Array<any>): void {
        this.logFormattedLogMessage(message, LogLevel.Error, args);
    }

    f(message: any, ...args: Array<any>): void {
        this.fatal(message, ...args);
    }

    fatal(message: any, ...args: Array<any>): void {
        this.logFormattedLogMessage(message, LogLevel.Fatal, args);
    }

    private logFormattedLogMessage(message: any, level: LogLevel, args: Array<any>): void {
        if (this.isConsoleOutputEnabled === false)
            return;

        if (args.length > 0)
            message = StringExpert.format(message, ...args);

        var prefix: string = this.getTimeStr() + ' ' + (this.omitLevel === true ? '' : `[${LogLevel[level].toUpperCase() }]`) + `${this._callerName }`;

        if (this.isStyleEnabled)
            console.log(
                //`%c${prefix}%c: ${message}`, 
                `%c${prefix}: ${message}`, 
                'color: ' + this.levelColorMap[level], 
                '');
        else
            console.log(`${prefix}: ${message}`);
    }

    setCaller(caller: Object|string): void {
        this._callerName = (typeof caller === 'string') ? caller : caller.constructor['name']; // 'name' is valid, ignore compile warning
    }

    private getTimeStr(): string {
        var now: Date = new Date();

        return StringExpert.format(
            "{0}:{1}:{2}",
            this.twoDigits(now.getHours()),
            this.twoDigits(now.getMinutes()),
            this.twoDigits(now.getSeconds())
            );
    }

    private twoDigits(n: number) {
        return `00${n}`.slice(-2);
    }

}