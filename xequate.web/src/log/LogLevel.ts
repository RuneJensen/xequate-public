export enum LogLevel {
	Info,
	Verbose,
    Debug,
    Error,
    Fatal
}
