/// <reference path="../../typings/tsd.d.ts" /> 
	
export class ValueChangeEvent<E extends Event, V> {
	event: E;
	value: V;
}
