/// <reference path="../typings/tsd.d.ts" /> 

import { bootstrap } from 'angular2/angular2';
import { App } from './app/app';
import { FocusLoopService, CurrencyImageService, UnitImageService, AppDataGateway, AppCategoryProvider } from './xequate/service';
// import { FocusLoopService, CurrencyImageService, UnitImageService, AppDataGateway } from './xequate/service';

bootstrap(
	App, 
	[
		FocusLoopService, 
		CurrencyImageService,
		UnitImageService,
		AppDataGateway,
		AppCategoryProvider
	]); //, [formInjectables]);