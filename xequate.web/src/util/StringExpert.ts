/// <reference path="../../typings/tsd.d.ts" />

export class StringExpert {
	static startsWith(str: string, subStr: string): boolean {
		return str.indexOf(subStr) === 0;
	}

	static format(str: string, ...args: Array<any>): string {
		return str.replace(
	    	/{(\d+)}/g, 
			(match, number) => (args[number] != undefined) ? args[number].toString() : 'undefined' // args[number].toString() : 'undefined' //match
	    	);
	}

	static areEqual(s1: string, s2 :string, isCaseSensitive: boolean) {
		if (isCaseSensitive)
			return s1 === s2
		else
			return s1.toLowerCase() === s2.toLowerCase()

		// if (s1 == undefined && s2 == undefined)
		// 	return true;

		// var caseInsensitiveFlag = "i";
		// var flag = (isCaseSensitive === false) ? caseInsensitiveFlag : undefined; 

		// return  s1 != undefined && 
		// 		s2 != undefined &&
		// 		s1.length === s2.length && 
		// 		s1.search(new RegExp(s2, flag)) === 0;
	}


	/*escapeRegExp(str: string): string {
	  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	}*/
	
}

