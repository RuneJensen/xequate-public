/// <reference path="../../typings/tsd.d.ts" />

export class CharCodeExpert {

    static Codes = {
        LEFT: 37,
        RIGHT: 39,
        UP: 38,
        DOWN: 40,
        TAB: 9,
        ENTER: 13,
        ESCAPE: 27,
        BACKSPACE: 8,
    }

    /*static skod() {
        for (var k in CharCodeExpert) {
            console.log(TypeExpert.isNumeric(CharCodeExpert[k]));
        }
    }*/
}
