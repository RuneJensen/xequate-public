/// <reference path="../../typings/tsd.d.ts" />

export interface CaretInfo {
	content: string;
	contentLength: number;
	selectLength: number;
	position: number;
	isSelecting: boolean;
	isAllSelected: boolean;			
	isFirstPosition: boolean;
	isLastPosition: boolean;
	isFocused: boolean;
}

export class CaretExpert {

	static selectContents(el: HTMLSpanElement): void {
		requestAnimationFrame(() => {
		        var range = document.createRange();
		        range.selectNodeContents(el);
		        var selection = window.getSelection();
		        selection.removeAllRanges();
		        selection.addRange(range);
		    });		
	}

	static setCaretPosition(contentElement: Node, position: number) {
		this.selectContent(contentElement, position, position);
	}
 
	static selectContent(contentElement: Node, start: number, end: number): void {
		var range = document.createRange();
		range.setStart(contentElement, start);
		range.setEnd(contentElement, end);
		var selection = window.getSelection();
		selection.removeAllRanges();
		selection.addRange(range);
	}

	static getCaretInfo(element: HTMLSpanElement | HTMLInputElement): CaretInfo {
	    if (window.getSelection == undefined)
	        throw 'not supported';

		var isSpan = element.tagName === 'SPAN';
		if (isSpan === false)
			throw 'Non-span tag support not implemented'
		else {
		    var isMultiLine = element.innerHTML.indexOf('\n') >= 0;
		    if (isMultiLine === true)
		    	throw 'Cannot handle multiline';
		}

		var content: string = isSpan === true? element.innerHTML : (<HTMLInputElement>element).value;

	    var selection: Selection = window.getSelection();
	    //var isSelecting = selection.type !== 'Range'

		if (selection.rangeCount === 0)
			return undefined;

	    var range: Range = selection.getRangeAt(0);
		var selectLength: number = range.toString().length; // *
		var preCaretRange: Range = range.cloneRange();
		preCaretRange.selectNodeContents(element);
		preCaretRange.setEnd(range.endContainer, range.endOffset);
		var position = preCaretRange.toString().length; 

		return {
			content: content,
			contentLength: content.length,
			selectLength: selectLength,
			position: position,
			isSelecting: selectLength > 0, 
			isAllSelected: selectLength === content.length,			
			isFirstPosition: position === 0,
			isLastPosition: position === content.length,
			isFocused: document.activeElement === element
		}
	}
}