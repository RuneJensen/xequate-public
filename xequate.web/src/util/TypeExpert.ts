/// <reference path="../../typings/tsd.d.ts" />

export class TypeExpert {
	static isNumber(obj: any): boolean {
		return typeof obj === 'number';
	}

    static isNumeric(maybeNumeric: any) {
        return /^[-+]?(\d+|\d+\.\d*|\d*\.\d+)$/.test(maybeNumeric);
    }

    static isObject(value: any) {
    	return typeof value === 'object';
    }
}