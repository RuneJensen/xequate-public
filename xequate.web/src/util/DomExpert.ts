/// <reference path="../../typings/tsd.d.ts" />

export class DomExpert {
	static getFirstContentEditable(parent: HTMLElement): HTMLElement {
		return <HTMLElement>parent.querySelector('[contenteditable]');
	}
}