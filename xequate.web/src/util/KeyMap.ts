/// <reference path="../../typings/tsd.d.ts" />

export interface KeyMap {
	[from: string]: number;
}