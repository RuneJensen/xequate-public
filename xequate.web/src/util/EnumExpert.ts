/// <reference path="../../typings/tsd.d.ts" />

export class EnumExpert {
    static getMembers<E>(e: E): Array<E> { // typescript unfortunately does not allow type checking on enums
        return this.getMemberValues(e).map((value) => e[value]);
    }

    static getMemberValues(someEnum): Array<number> { // typescript unfortunately does not allow type checking on enums
        var result: Array<number> = [];
        for (let member in someEnum) {
            var memberValue: number = parseInt(member, 10); // parse as radix 10
            var isValueProperty = (memberValue >= 0);
            if (isValueProperty)
                result.push(memberValue);
        }

        return result;
    }
}




