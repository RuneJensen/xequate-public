/// <reference path="../../typings/tsd.d.ts" />

export class MathExpert {

    // In line with the prevalent definition of mod.
    static mod(n1: number, n2: number): number {
        return ((n1 % n2) + n2) % n2;
    };

    static round(value: number, decimalCount: number): number {
		return Number(value.toFixed(decimalCount));
    }
}