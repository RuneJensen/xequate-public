/// <reference path="../../typings/tsd.d.ts" />

export class EventExpert {

	static simulateKeydown(keyCode: number, target: Element): void {
		if (document.createEvent == undefined)
			throw 'not supported'; 

		var eventObj: KeyboardEvent = <KeyboardEvent>document.createEvent("Events");
		eventObj.initEvent("keydown", true, true);
		eventObj.which = keyCode;
		eventObj.keyCode = keyCode;
		target.dispatchEvent(eventObj);
	}

}