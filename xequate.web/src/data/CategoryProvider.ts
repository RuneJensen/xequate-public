/// <reference path="../../typings/tsd.d.ts" />

import {EventEmitter} from 'angular2/angular2';
import {State, DataGatewayState} from '../xequate/data';


export interface CategoryProvider {
	categories: Array<any>;
	categoryKeys: Array<any>;
	getFirstCategoryByKey(key: string): any;
	doesCategoryKeyExist(key: string): boolean;
	isCurrencyDataAvailable: boolean;
	currencyDataUpdated: EventEmitter;
    state: State<DataGatewayState>;
}