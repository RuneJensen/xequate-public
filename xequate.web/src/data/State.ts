export interface State<Key> {
    contains(state: Key): boolean;
    add(state: Key): void;
    remove(state: Key): void;
    keys: Array<Key>;
}
