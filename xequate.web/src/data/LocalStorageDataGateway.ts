import {EquationState} from './EquationState';
import {CategoryData} from './CategoryData';
import {Logger, AppLogger} from '../xequate/log';
import {StringExpert} from '../xequate/util';

const BUILD_MAJOR_VERSION = CONFIGURATION.BUILD_MAJOR_VERSION;
const STORAGE_PREFIX = "xequate";
const STORAGE_KEY_PREFIX = STORAGE_PREFIX + "_v" + BUILD_MAJOR_VERSION;

const STORAGE_KEY_EQUATION_STATE = STORAGE_KEY_PREFIX + "_cache_equation_state";
const STORAGE_KEY_CATEGORY_DATA = STORAGE_KEY_PREFIX + "_cache_category_data";


// TODO rename as local storage related
export class LocalStorageDataGateway {  
	protected log: Logger = new AppLogger(this);


    constructor() {
        this.log.v('construct');

        this.removeDeprecatedStorageKeys(3000);
    }

    private removeDeprecatedStorageKeys(delayMilliSeconds: number): void {
		setTimeout(
			() => {
				this.log.d('cleaning up localstorage..');

				var removeCount = 0;
				for(var key in localStorage) {
					if (this.doesKeyBelongToAppButNotCurrentVersion(key)) {
						localStorage.removeItem(key);
						removeCount++;

						this.log.i("removed deprecated localstorage key: "  + key);
					}
				}

				this.log.d("localstorage cleanup, number of deprecated (removed) storage keys: " + removeCount);
			}, 
			delayMilliSeconds
		);
    }

    private doesKeyBelongToAppButNotCurrentVersion(key) {
    	return StringExpert.startsWith(key, STORAGE_PREFIX) == true &&
    		   StringExpert.startsWith(key, STORAGE_KEY_PREFIX) == false;
    }

	get categoryData(): CategoryData {
		return <CategoryData>JSON.parse(localStorage.getItem(STORAGE_KEY_CATEGORY_DATA));
	}

	set categoryData(value: CategoryData) {
    	localStorage.setItem(STORAGE_KEY_CATEGORY_DATA, JSON.stringify(value));
	}
	
	get equationState(): EquationState {
		return <EquationState>JSON.parse(localStorage.getItem(STORAGE_KEY_EQUATION_STATE));
	}

	set equationState(value: EquationState) {
    	localStorage.setItem(STORAGE_KEY_EQUATION_STATE, JSON.stringify(value));
	}

}
