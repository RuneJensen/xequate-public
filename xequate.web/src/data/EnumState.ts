/// <reference path="../../typings/tsd.d.ts" />

import {AppLogger, Logger} from '../xequate/log';
import {EnumExpert} from '../xequate/util';
import {State} from '../xequate/data';

//var EnumExpert = Experts.EnumExpert;

export class EnumState<Enum> implements State<Enum> {
    private log: Logger = new AppLogger(this);

    private _state: number = 0;
    //private log: Logger = ..

    constructor(private sourceEnum: Object) {  // compromising slightly on type safety to achieve convenience
        //log.setCaller(this);
    }

    // Interface implementations

    contains(state: Enum): boolean {
        var stateNumber: number = Math.pow(2, <number><any>state);

        return (this._state & stateNumber) === stateNumber;
    }

    add(state: Enum): void {
        var stateNumber: number = Math.pow(2, <number><any>state);
        this._state = this._state | stateNumber;

        this.log.d(`State change: ${JSON.stringify(this.keys) } (added: "${this.sourceEnum[<number><any>state]}")`);
    }

    remove(state: Enum): void {
        var stateNumber: number = Math.pow(2, <number><any>state);
        var allOtherStateNumbers: number = ~stateNumber;

        this._state = this._state & allOtherStateNumbers;  // since each state will be associated with a single bit, XOR will do the job of removing a state

        this.log.d(`State change: ${JSON.stringify(this.keys) } (removed: "${this.sourceEnum[<number><any>state]}")`);
    }

    get keys(): Array<Enum> {
        return EnumExpert.getMemberValues(this.sourceEnum).filter((keyValue: number) => {
            var stateNumber: number = Math.pow(2, keyValue);
            var isPartOfStateSet = (this._state & stateNumber) === stateNumber;

            return isPartOfStateSet;
        }).map((keyValue) => this.sourceEnum[keyValue]);
    }

}
