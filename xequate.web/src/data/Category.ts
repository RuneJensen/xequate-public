export interface Category {
	name: string;
	baseKey: string;
	map: {
		[index: string]: number
	}	
}