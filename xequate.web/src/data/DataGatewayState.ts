export enum DataGatewayState {  // associated values become 1,2,.. 
    HasCachedData,
    HasUpdatedData,
    IsUpdatingData,
    DataFetchFailed
}

