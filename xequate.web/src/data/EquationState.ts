/// <reference path="../../typings/tsd.d.ts" />

import {CategoryItem} from './CategoryItem';


export class EquationState {
	leftCategoryItem: CategoryItem;
	rightCategoryItem: CategoryItem;

	leftValue: number;
	rightValue: number;
}