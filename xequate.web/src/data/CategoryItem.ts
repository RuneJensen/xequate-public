import {Category} from 'Category';

export interface CategoryItem {
	key: string;
	category: Category;
}