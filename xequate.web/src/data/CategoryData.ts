/// <reference path="../../typings/tsd.d.ts" />

import {Category} from './Category';
import {CategoryItem} from './CategoryItem';


export interface CategoryData {
    categories: Array<Category>;
    categoryKeys: Array<CategoryItem>;
}
