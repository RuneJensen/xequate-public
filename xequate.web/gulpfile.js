var gulp = require('gulp');
var open = require('gulp-open');
var connect = require('gulp-connect');
var sass = require('gulp-sass');
var notifier = require('node-notifier');
var ts = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var runSequence = require('run-sequence');
var del = require('del');
var browserify = require('browserify');
var transform = require('vinyl-transform');
//var source = require('vinyl-source-stream');
var uglify = require('gulp-uglify');
// var through2 = require('through2');
var rename = require("gulp-rename");
var ifThen = require('gulp-if');
var flatten = require('gulp-flatten');
var inject = require('gulp-inject');

var config = require('./gulpfile.config');

var TypescriptDef = require('./node_modules/typescript/');

var tsProject = ts.createProject('./tsconfig.json', { 
	typescript: require('./node_modules/typescript/')
});


//console.log(tsProject.src())

	
gulp.task('connect', function() {
    connect.server({
        root: config.serveDir,
        port: config.devPort,
        livereload: true
    });
});


gulp.task('open', function(){
    var options = {
        url: 'http://localhost:' + config.devPort,
    };
    gulp.src('./index.html')
        .pipe(open('', options));
});


/*
gulp.task('notify', function() {
	notifier.notify({ title: 'FAILED: sass transpilation ', message: 'See console output for details.' });
});
*/

gulp.task('serve', [
	'connect', 
	'open', 
	'watch'
	]);


gulp.task('traceur', function () {
    return gulp.src('src/**/*.ts')
        .pipe(traceur())
        .pipe(gulp.dest('build'));
});


gulp.task('build:copy-src', function () {
    return gulp
        .src(['./src/**/*'])
        .pipe(gulp.dest(config.buildDir));
});


gulp.task('copy-lib', function () {
    return gulp
        .src(['./lib/**/*'])
        .pipe(gulp.dest(config.buildDir + '/lib'));
});

gulp.task('build:copy-src', function () {
    return gulp
        .src(['./src/**/*'])
        .pipe(gulp.dest('./build'));
});


gulp.task('ts-transpile-copy', function (cb) {
    return gulp
      .src(['./src/**/*.ts'])
	  .pipe(ts(tsProject))
      //.pipe(sourcemaps.init())
      /*.pipe(ts({
          declaration: false,
          typescript: TypescriptDef,
          target: 'ES5',
          experimentalDecorators: true,
          emitDecoratorMetadata: true,
          noEmitHelpers: false, //true,
          module: 'commonjs',  // amd
          sortOutput: true,
          //noExternalResolve: true, 
          //sourceMap: true,
          //declarationFiles: true
          //out: 'out.js'
      })).js*/
      //.pipe(browserify({ insertGlobals: true }))
      //.pipe(concat('app.js'))
      //.pipe(sourcemaps.write())
      //.pipe(flatten())
      .pipe(gulp.dest(config.buildDir));
});


gulp.task('build:transpile-clean', function (cb) {
    del([config.buildDir + '/**/*.{ts,scss}'], cb);
});


gulp.task('build:flatten', function (cb) {
    return gulp.src([config.buildDir + '/**/*'])
        .pipe(flatten())
        .pipe(gulp.dest(config.buildDir));
});


gulp.task('build:flatten-clean', function (cb) {
    del([config.buildDir + '/*/'], cb);
});


gulp.task('build:concatenate', function () {
    // transform regular node stream to gulp (buffered vinyl) stream
    var browserified = transform(function (filename) {
        var b = browserify({
            entries: filename,
            //nobuiltins: 'angular2/*',
            debug: true,
            //insertGlobals: true,
			//standalone: true
        });
        if (config.ignoreModulesNames != undefined)
            b.ignore(config.ignoreModulesNames);
        return b.bundle();
    });


    var filepath = config.buildDir + '/' + config.entryFilename;

    return gulp
      .src(filepath)
      .pipe(browserified)
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(ifThen(config.isRelease, uglify()))
      //.pipe(rename(config.appFilename))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(config.buildDir));
});


gulp.task('build:concatenate-clean', function (cb) {
    //del([config.buildDir + '/*/'], cb);
    del([
        config.buildDir + '/*',
        '!' + config.buildDir + '/*.{html,css,map}',
        '!' + config.buildDir + '/' + config.entryFilename,
    ], cb);
});




// delete
gulp.task('build:clean-temp', function (cb) {
    del([
        config.buildDir + '/**/*',
        '!' + config.buildDir + '/' + config.appFilename
    ], cb)
});


gulp.task('build-release', function (cb) {
    config.isRelease = true;

    return runSequence('build');
});


gulp.task('build-debug', function (cb) {
    config.isRelease = false;

    return runSequence('build');
});


/*
gulp.task('default', function(cb) {
  runSequence('clean',
      'build:ng2',
      'build:shim',
      'build:strip_maps',
      cb);
});
*/


/**
 * Generates the app.d.ts references file dynamically from all application *.ts files.
 */
gulp.task('gen-ts-refs', function () {
    var target = gulp.src('./typings/typescriptApp.d.ts');
    var sources = gulp.src(['src/**/*.ts'], {read: false});
    return target.pipe(inject(sources, {
        starttag: '//{',
        endtag: '//}',
        transform: function (filepath) {
            return '/// <reference path="..' + filepath + '" />';
        }
    })).pipe(gulp.dest('./typings/'));
});


gulp.task('compile-ts', function () {
    var sourceTsFiles = ['./src/**/*.ts',                //path to typescript files
                         './typings/**/*.ts', //reference to library .d.ts files
                         './typings/typescriptApp.d.ts'];     //reference to app.d.ts files

    var tsResult = gulp.src(sourceTsFiles)
                       .pipe(sourcemaps.init())
					   .pipe(ts(tsProject));
                       /*.pipe(ts({
							experimentalDecorators: true,
							emitDecoratorMetadata: true,
							declaration: false,
							typescript: TypescriptDef,
							target: 'ES5',
							experimentalDecorators: true,
							emitDecoratorMetadata: true,
							noEmitHelpers: false, //true,
							module: 'commonjs',  // amd
							//out: './build2/teeeeeeest.js',
							sortOutput: true,
							//                           target: 'ES5',
							declarationFiles: false,
							noExternalResolve: true
                       }));*/
					   

        tsResult.dts.pipe(gulp.dest('./build2'));
        return tsResult.js
                        .pipe(sourcemaps.write('.'))
                        .pipe(gulp.dest('./build2'));
});


gulp.task('clean', function (cb) {
    //  del(['**'], {cwd: 'app'}, cb)
    del([
        config.tempDir,  // + '/**'], cb)
        config.buildDir
    ],cb);
});


gulp.task('ts-transpile-copy-concat', function (cb) {
	runSequence(
        'ts-transpile-copy',
        'build:concatenate',
        'build:concatenate-clean',
        cb
        );
});


gulp.task('sass-transpile-copy', function() {
	var sassInstance = sass({
            errLogToConsole: true
        });
	sassInstance.on('error', function(e) { 
		notifier.notify({ title: 'FAILED: sass transpilation ', message: 'See console output for details.' });  
		console.log('Sass compilation failed:\n ' + e + '\n');
		sassInstance.end();
	});
    gulp.src('./src/**/*.scss') // recursive
        .pipe(sassInstance)
        .pipe(flatten())
        .pipe(gulp.dest('./build'));
});


gulp.task('html-copy', function () {
    return gulp
        .src(['./src/**/*.html'])
        .pipe(flatten())
        .pipe(gulp.dest(config.buildDir));
});


gulp.task('watch',function() {
    gulp.watch('src/**/*.scss', ['sass-transpile-copy']);
    gulp.watch('src/**/*.html', ['html-copy']);
    //gulp.watch('src/**/*.ts', ['ts-transpile-copy-concat']);
});


gulp.task('compile-ok', function() {
  console.log('Compile completed');
});


gulp.task('build', function (cb) {
    console.log('\n*********************');
    console.log('*** This is ' + (config.isRelease === true? 'RELEASE' : 'DEBUG') + ' ***');
    console.log('*********************\n');

    runSequence(
        ['clean', 'gen-ts-refs'],
        ['html-copy', 'sass-transpile-copy', 'ts-transpile-copy-concat'],
        'copy-lib',
        'compile-ok',
        cb
		);
});

