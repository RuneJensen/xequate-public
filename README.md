A simple currency and unit converter, based on the alpha 28 pre-release of Googles Angular2 framework. 

Angular2 allows web developers to adopt a more declarative coding syntax. There's no longer any need to explicitly hotwire the DOM, e.g. with jQuery - which has essentially always been a bad, but necessary practice.

This project demonstrates the even-driven manner of this: both sides of the equation may be manipulated and a custom dropdown allows for autocompletion of currency or unit codes. There is no fiddling with DOM elements, except special cases that will be eliminated once Google completes the framework.

Webpack is used for building the project in a modular style, with some Gulp dependencies/tasks retained for development purposes. Bootstrap facilitates a mobile-first approach.

The project uses the (as of June 1, 2015) [recommended project structure](https://github.com/angular-class/angular2-webpack-starter#file-structure) and I've settled on the majority of the Typescript conventions as outlined by the [Typescript wiki](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines).

~~Note that the upcoming Angular framework is still in its inception, changes frequently and is loosely documented (there is no production or beta build of Angular2).~~ Angular2 is now in RC4 as of September 2016.

## The power of a declarative approach

The user cannot equate two different types of input (again, there is no explicit javascript involved here): 

![Alt text](/README/ss1.png?raw=true "Screenshot 1")

The user is made aware of invalid input in a non-obstructive manner:

![Alt text](/README/ss2.png?raw=true "Screenshot 2")

Both of these examples use a tooltip, a brainchild I accomplished using CSS/pseudo-classes only. By turning this into an Angular2 directive, it becomes completely modular and can be applied by simply specifying an attribute in the html markup:

![Alt text](/README/ss3.png?raw=true "Screenshot 3")

This is in stark contrast to existing solutions, which rely on explicit (and messy) javascript. Note the customization by attributes. This is accomplished without any scripting, by matching combinations of css classes - produced entirely through Sass logic ([tool-tip.scss](xequate.web/src/app/decorators/tool-tip.scss)). The benefit is that it renders based on CSS rather than script - and is therefore FAST! The tradeoff is a chunk of css with size exponential in the number of options, but this remains relatively low for a small set of options.
 