## Python Flask project for verifying rates available using the Google Cloud platform.

Based on the [Flask micro framework](http://flask.pocoo.org).

This project runs in the Google Cloud and verifies daily that rates are up to date. If not, an admin is notified by email.
