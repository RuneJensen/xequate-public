#import urllib
import urllib2
import json
import time
import datetime

def checkLastestEntry():
	response = urllib2.urlopen('https://fiery-heat-5927.firebaseio.com/rates.json?print=pretty&limitToLast=1&orderBy="$key"')
	jsonString = response.read()
	root = json.loads(jsonString)

	print 'Received:\n'
	print root
	print '\n'
	
	assert len(root.keys()) > 0, 'Latest rate entry has no keys'
	
	timestampString =  root.keys()[0]
	assert int(timestampString) > 0, 'The rate entry timestamp was not a strictly positive number'
	
	rate = root[timestampString]
	createdKey = 'created'
	assert createdKey in rate, 'Rate entry does not have \'created\' key'
	createdSeconds = rate[createdKey]
	
	entryAgeSeconds =  time.time() - createdSeconds #datetime.datetime.fromtimestamp()
	
	maxAgesHours = 24
	assert entryAgeSeconds < maxAgesHours * 60 * 60, 'Latest rate entry is more than %s hours old' % maxAgesHours
	
	entryAgeMinutes = entryAgeSeconds/60
	print 'Last entry age in minutes: %s' % entryAgeMinutes 
	
	return entryAgeMinutes
	
	#return root

