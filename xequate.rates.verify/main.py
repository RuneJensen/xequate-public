from flask import Flask
import core
import os
import time
import datetime
from google.appengine.api import mail

app = Flask(__name__)
app.config['DEBUG'] = True

# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

email = '';

def sendErrorMail(msg):
	mail.send_mail(email, email, 'Currency rates monitoring service', msg)	

	
@app.route('/')
def hello():
	return 'Hello'
	
	
@app.route('/verify')
def verify():
	try:
		ageMinutes = core.checkLastestEntry();
		return 'Relax, the latest rate entry is %s minutes old' % int(ageMinutes)
	except Exception as e:
		twoHoursSeconds = 2 * 60 * 60
		timeString = datetime.datetime.fromtimestamp(time.time() + twoHoursSeconds).strftime('%X %x %Z')
		errorMsg = 'Error at %s: %s' % (timeString,e)
		sendErrorMail(errorMsg)
		return errorMsg


@app.errorhandler(404)
def page_not_found(e):
	"""Return a custom 404 error."""
	return 'Sorry, nothing at this URL.', 404
