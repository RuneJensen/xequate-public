# Ensures that a json is structured as expected
class Verifier:
	def ensureAdheresToCategoryFormat(self, category):
		categoryAttributes = ([
			'name', 
			'timestamp',
			'createTime', 
			'createTimeFormatted', 
			'map'
			])
		for categoryAttribute in categoryAttributes:
			assert categoryAttribute in category, 'category attribute \'{0}\' was not present'.format(categoryAttribute)
		