from xequate_core import LogExpert
import urllib2
import json
import time
import datetime
from . import Verifier


class CurrencyExpert:
	KEY_TIMESTAMP = 'timestamp'
	KEY_BASE = 'base'
	KEY_RATES = 'rates'	
	KEY_DISCLAIMER = 'disclaimer'
	KEY_LICENSE = 'license'


	def __init__(self, log=None):
		self.log = (log if log is not None else LogExpert.getConsoleLogger())


	def __ensureOpenExchangeResponseJsonIsValid(self, root):
		# Ensure root is not empty
		assert len(root.keys()) > 0, 'json root is empty'

		# Ensure the base currency code is present
		assert self.KEY_BASE in root, 'json root does not contain the base currency'		
		
		# Ensure the rates are present and not the empty set
		assert self.KEY_RATES in root, 'json root does not contain a \'rates\' key'		
		rates = root[self.KEY_RATES]
		ratesCount = len(rates.keys())
		assert ratesCount > 0, 'rates json had zero entries'


	def getCurrencyJson(self, openExchangeAppId):
		response = urllib2.urlopen('https://openexchangerates.org/api/latest.json?app_id=%s' % openExchangeAppId)
		html = response.read()
		
		root = json.loads(html)
		self.__ensureOpenExchangeResponseJsonIsValid(root)
		
		# Remove unwanted keys
		keysToRemove = [self.KEY_DISCLAIMER, self.KEY_LICENSE]	
		for key in keysToRemove:
			if key in root:
				del root[key]

		# Rename certain keys
		keysToRename = [(self.KEY_BASE, 'baseKey'), (self.KEY_RATES, 'map')]	
		for keyPair in keysToRename:
			fromKey = keyPair[0] 
			toKey = keyPair[1] 
			assert fromKey in root, 'Could not perform rename {0} as key \'{1}\' as it did not exist'.format(keyPair, fromKey)
			root[toKey] = root[fromKey]
			del root[fromKey]

		# Ensure the timestamp makes sense
		assert self.KEY_TIMESTAMP in root
		timestamp = root[self.KEY_TIMESTAMP]
		olderTimestamp = 1433664060
		assert timestamp > olderTimestamp, 'Timestamp was suspicious (failed: %s > %s)' % (timestamp, olderTimestamp)

		# Inject timestamps to indicate persist time
		#del root[KEY_TIMESTAMP]
		currentTime = time.time()
		root['createTime'] = int(currentTime)
		root['createTimeFormatted'] =  datetime.datetime.fromtimestamp(currentTime).strftime('%Y-%m-%d %H:%M:%S')

		root['name'] = 'Currency'

		Verifier().ensureAdheresToCategoryFormat(root)

		return root

	
	# def getRatesAndPublish(self, openExchangeAppId, firebaseAuthKey, firebaseAppId):
	# 	ratesJson = self.getRatesJson(openExchangeAppId)
	# 	timestamp = ratesJson[self.KEY_TIMESTAMP]
	# 	writeLocations = ['history/currency/%s' % timestamp, 'latest/currency'];
	# 	response = self.firebaseInsertOrUpdate(ratesJson, firebaseAuthKey, firebaseAppId, writeLocations)
	# 	return response


	# def getUnitsAndPublish(self, openExchangeAppId, firebaseAuthKey, firebaseAppId):
	# 	ratesJson = self.getRatesJson(openExchangeAppId)
	# 	timestamp = ratesJson[self.KEY_TIMESTAMP]
	# 	writeLocations = ['history/currency/%s' % timestamp, 'latest/currency'];
	# 	response = self.firebaseInsertOrUpdate(ratesJson, firebaseAuthKey, firebaseAppId, writeLocations)
	# 	return response



