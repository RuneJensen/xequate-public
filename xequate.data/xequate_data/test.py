import unittest
import sys
import json 

sys.path.append('..')
from xequate_data import *

from xequate_core import ConfigExpert, LogExpert

log = LogExpert.getConsoleLogger()


class Test(unittest.TestCase):
	def setUp(self):
		print '**** Test setup ****\n'	


	def tearDown(self):
		pass


	def test_get_currency_json(self):
		config = ConfigExpert().getConfigFromFileOrInput()

		currencyJson = CurrencyExpert(log=log).getCurrencyJson(config['open_exchange_app_id'])
		
		serializedLength = len(json.dumps(currencyJson))
		assert serializedLength > 2000, 'serialised json length was suspicious (did not exceed 2000 characters)'


	def test_get_unit_json(self):
		unitJson = UnitExpert(log=log).getUnitJson()
		#log.info('units: \n' + json.dumps(unitJson, indent=True))

	

if __name__ == '__main__':
    unittest.main()