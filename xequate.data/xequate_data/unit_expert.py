from xequate_core import LogExpert
import json
import os.path
import inspect
import time
import datetime
from os.path import abspath, dirname
from . import Verifier


class UnitExpert:
	unitFilesDir = 'unit_expert_unit_definitions'
	unitFilenames = ([
		'area.json',
		'digital_storage.json',
		'fuel consumption.json',
		'length.json',
		'mass.json',
		'speed.json',
		'temperature.json',
		'time.json',
		'volume.json'
		])

	def __init__(self, log=None):
	    if log is None:
			self.log = LogExpert.getConsoleLogger()
	    else:
	    	 self.log = log

	def getUnitJson(self):
		self.log.info('compiling units')
		moduleDir = dirname(abspath(__file__))
		result = []
		for (index, filename) in enumerate(self.unitFilenames):
			filepath = "{0}/{1}/{2}".format(moduleDir, self.unitFilesDir, filename)
			fileExists = os.path.isfile(filepath)
			assert fileExists, 'Unit definition file \'{0}\' does not exist'.format(filepath)

			self.log.info('processing file {0}/{1} \'{2}\''.format(index+1, len(self.unitFilenames), filename))

			with open(filepath) as jsonFile: 
				fileJson = json.load(jsonFile)
				assert 'name' in fileJson, 'no unit name specified'
				unitName = fileJson['name']
				map = fileJson['map']
				baseKey = fileJson['baseKey']
				assert baseKey in map, 'base key \'{0}\' is not present in the map for unit \'{1}\''.format(baseKey, unitName)

				currentTime = time.time()
				fileJson['timestamp'] = int(currentTime)
				fileJson['createTime'] = int(currentTime)
				fileJson['createTimeFormatted'] =  datetime.datetime.fromtimestamp(currentTime).strftime('%Y-%m-%d %H:%M:%S')

				Verifier().ensureAdheresToCategoryFormat(fileJson)

				result.append(fileJson)

		return result

