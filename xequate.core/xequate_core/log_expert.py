import sys
import logging
from logging import config


class LogExpert:
	@staticmethod
	def getConsoleLogger():
		LOGGING = {  
		    'version': 1,
		    'formatters': {
		        'verbose': { 'format': '%(levelname)s %(module)s P%(process)d T%(thread)d %(message)s' },
		        'custom': { 
		        	'format': '%(asctime)s %(levelname)s %(module)s: %(message)s',
		        	'datefmt': '%Y-%m-%d %H:%M:%S' 
		        	},
		        },
		    'handlers': {
		        'stdout':{
		            #'level': logging.DEBUG,
		            'class':'logging.StreamHandler',
		            'stream': sys.stdout, 	# Replace strm with stream if you're using python 2.7.
		            'formatter': 'custom'
		        }
		    },
		    'loggers': {
		        'console': {
		            'handlers': ['stdout'],
		            'level': logging.DEBUG,
		            'propagate': True,
		        	},
		    	}		
			}
		config.dictConfig(LOGGING)

		consoleLogger = logging.getLogger("console")
		return consoleLogger

