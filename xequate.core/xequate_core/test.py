import unittest
import sys
import StringIO

sys.path.append('..')
from xequate_core import *


class Test(unittest.TestCase):
	def setUp(self):
		#print '**** Test setup ****\n'	
		pass

		
	def tearDown(self):
		# print '\n**** Test teardown ****'	
		pass


	def test_get_console_logger_using_default_file(self):
		LogExpert().getConsoleLogger()


	def test_get_config_using_filename(self):
		ConfigExpert().getConfigFromFilename('../../xequate.debug.config.json')


	def test_get_console_logger_using_std_in(self):
		sys.stdin = StringIO.StringIO("""
			{
				"open_exchange_app_id": "_",
				"firebase_auth_key": "_",
				"firebase_app_id": "_"
			}
			""")
		ConfigExpert().getConfigFromFileOrInput()
		

	def test_get_config(self):
		ConfigExpert().getConfigFromFileOrInput()
	

if __name__ == '__main__':
    unittest.main()
