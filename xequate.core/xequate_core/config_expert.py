import os
import sys
import json
from . import LogExpert

class ConfigExpert:
	CONFIG_FILENAME = 'xequate.config.json'
	CONFIG_REQUIRED_KEYS = ['open_exchange_app_id', 'firebase_auth_key', 'firebase_app_id']
	
	def __init__(self, log=None):
		self.log = (log if log is not None else LogExpert().getConsoleLogger())
		#pass


	def __ensureConfigIsValid(self, config):
		for key in self.CONFIG_REQUIRED_KEYS:
			assert config[key], 'No configuration present for key \'{0}\''.format(key)
		return None


	def getConfigFromFilename(self, filepath):
		with open(filepath) as jsonFile: 
			config = json.load(jsonFile)
		self.__ensureConfigIsValid(config)
		return config


	def getConfigFromJsonStr(self, configJsonStr):
		config = json.loads(configJsonStr)
		self.__ensureConfigIsValid(config)
		return config
		

	def getConfigFromFileOrInput(self):
		isInputProvided = (sys.stdin.isatty() == False)

		isConfigFilePresent = os.path.isfile(self.CONFIG_FILENAME)

		assert isInputProvided or isConfigFilePresent, 'No configuration input provided and config file \'%s\' is not present' % self.CONFIG_FILENAME

		if isInputProvided:
			self.log.info('Configuration provided as direct input')
			configJsonStr = sys.stdin.read()
		else:
			self.log.info('Configuration provided as file')
			configJsonStr = open(self.CONFIG_FILENAME).read()
		
		return self.getConfigFromJsonStr(configJsonStr)


