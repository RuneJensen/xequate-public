## Python Flask Project for updating rates using the Google Cloud platform.

Based on the [Flask micro framework](http://flask.pocoo.org).

This project runs in the Google Cloud and updates rates on a daily basis, persisting them in a Firebase database.

A configuration file needs to be present, specifying the open exchange app ID, as well as the firebase secret auth key.
