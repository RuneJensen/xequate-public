#import urllib
import urllib2
import json
import requests
import time
import datetime
import sys
import json
import os.path
import logging
# import logging.handlers
from logging import config


class LogExpert:
	@staticmethod
	def getConsoleLogger():
		LOGGING = {  
		    'version': 1,
		    'formatters': {
		        'verbose': { 'format': '%(levelname)s %(module)s P%(process)d T%(thread)d %(message)s' },
		        'custom': { 
		        	'format': '%(asctime)s %(levelname)s %(module)s: %(message)s',
		        	'datefmt': '%Y-%m-%d %H:%M:%S' 
		        	},
		        },
		    'handlers': {
		        'stdout':{
		            #'level': logging.DEBUG,
		            'class':'logging.StreamHandler',
		            'stream': sys.stdout, 	# Replace strm with stream if you're using python 2.7.
		            'formatter': 'custom'
		        }
		    },
		    'loggers': {
		        'console': {
		            'handlers': ['stdout'],
		            'level': logging.DEBUG,
		            'propagate': True,
		        	},
		    	}		
			}
		config.dictConfig(LOGGING)

		consoleLogger = logging.getLogger("console")
		return consoleLogger


class ConfigExpert:
	CONFIG_FILENAME = 'xequate.config.json'
	CONFIG_REQUIRED_KEYS = ['open_exchange_app_id', 'firebase_auth_key', 'firebase_app_id']
	
	def __init__(self, log=None):
	    if log is None:
    		self.log = LogExpert.getConsoleLogger();
	    else:
        	 self.log = log

	def getConfigFromFileOrInput(self):
		isInputProvided = (sys.stdin.isatty() == False)

		isConfigFilePresent = os.path.isfile(self.CONFIG_FILENAME)

		assert isInputProvided or isConfigFilePresent, 'No configuration input provided and config file \'%s\' is not present' % self.CONFIG_FILENAME

		if isInputProvided:
			self.log.info('Configuration provided as direct input')
			configJson = sys.stdin.read()
		else:
			self.log.info('Configuration provided as file')
			configJson = open(self.CONFIG_FILENAME).read()
		
		#print configJson
		config = json.loads(configJson)

		for key in self.CONFIG_REQUIRED_KEYS:
			assert config[key], 'No configuration present for key \'{0}\''.format(key)

		return config


class RatesExpert:
	KEY_TIMESTAMP = 'timestamp'

	def __init__(self, log=None):
	    if log is None:
			self.log = LogExpert.getConsoleLogger();
	    else:
	    	 self.log = log


	def ensureRatesJsonIsValid(self, root):
		# Ensure root is not empty
		assert len(root.keys()) > 0, 'json root is empty'

		# Ensure the base currency code is present
		baseKey = 'base'
		assert baseKey in root, 'json root does not contain the base currency'		
		
		# Ensure the rates are present and not the empty set
		ratesKey = 'rates'
		assert ratesKey in root, 'json root does not contain a \'rates\' key'		
		rates = root[ratesKey]
		ratesCount = len(rates.keys())
		assert ratesCount > 0, 'rates json had zero entries'


	def getRatesJson(self, openExchangeAppId):
		url = 'https://openexchangerates.org/api/latest.json?app_id=%s' % openExchangeAppId
		print url
		response = urllib2.urlopen(url)
		html = response.read()
		
		root = json.loads(html)
		self.ensureRatesJsonIsValid(root)
		
		# Remove unwanted keys
		keysToRemove = ['disclaimer', 'license']	
		for key in keysToRemove:
			if key in root:
				del root[key]

		# Rename certain keys
		keysToRename = [('base', 'baseKey'), ('rates', 'map')]	
		for keyPair in keysToRename:
			fromKey = keyPair[0] 
			toKey = keyPair[1] 
			assert fromKey in root, 'Could not perform rename {0} as key \'{1}\' as it did not exist'.format(keyPair, fromKey)
			root[toKey] = root[fromKey]
			del root[fromKey]

		# Ensure the timestamp makes sense
		assert self.KEY_TIMESTAMP in root
		timestamp = root[self.KEY_TIMESTAMP]
		olderTimestamp = 1433664060
		assert timestamp > olderTimestamp, 'Timestamp was suspicious (failed: %s > %s)' % (timestamp, olderTimestamp)

		# Inject timestamps to indicate persist time
		#del root[KEY_TIMESTAMP]
		currentTime = time.time()
		root['createTime'] = int(currentTime)
		root['createTimeFormatted'] =  datetime.datetime.fromtimestamp(currentTime).strftime('%Y-%m-%d %H:%M:%S')

		root['name'] = 'Currency'

		return root
	
	def firebaseInsertOrUpdate(self, dataJson, firebaseAuthKey, firebaseAppId, writeLocations):
		# Store rates to NoSQL provider
		for (index, writeLocation) in enumerate(writeLocations): 
			self.log.info('Storing data to location {0}/{1} (\'{2}\')..'.format(index +1, len(writeLocations), writeLocation))
			
			"""
			firebaseUrl = '..url..'
			db = firebase.FirebaseApplication(firebaseUrl, None)
					
			print 'Authenticating..'
			db.authentication = firebase.FirebaseAuthentication('.......auth........', None)
			
			
			#result = db.post('/rates/%s' % timestamp, None, None, None) #{'print': 'pretty'}
			result = db.put(url='/rates', name=timestamp, data=dataJson, connection=None, params={'print': 'pretty'}, headers={'X_FANCY_HEADER': 'VERY FANCY'})
			"""
			
			
			#postData = '{ "name": ".." }' #urllib.urlencode(jsonData)
			
			postUrl = 'https://{0}.firebaseio.com/{1}.json?auth={2}'.format(firebaseAppId, writeLocation, firebaseAuthKey) 
			postData = json.dumps(dataJson) # urllib.urlencode(jsonData)
			
			r = requests.put(postUrl, data=postData) # put request - creates or updates
			result = r.content
			self.log.debug('response: \n' + result)
			#return result		
		
		return 'ok'
		
	
	def getRatesAndPublish(self, openExchangeAppId, firebaseAuthKey, firebaseAppId):
		ratesJson = self.getRatesJson(openExchangeAppId)
		timestamp = ratesJson[self.KEY_TIMESTAMP]
		writeLocations = ['history/currency/%s' % timestamp, 'latest/currency'];
		response = self.firebaseInsertOrUpdate(ratesJson, firebaseAuthKey, firebaseAppId, writeLocations)
		return response


	def getUnitsAndPublish(self, openExchangeAppId, firebaseAuthKey, firebaseAppId):
		ratesJson = self.getRatesJson(openExchangeAppId)
		timestamp = ratesJson[self.KEY_TIMESTAMP]
		writeLocations = ['history/currency/%s' % timestamp, 'latest/currency'];
		response = self.firebaseInsertOrUpdate(ratesJson, firebaseAuthKey, firebaseAppId, writeLocations)
		return response



