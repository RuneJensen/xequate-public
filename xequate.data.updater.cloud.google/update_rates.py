import appengine_config
import os
import unittest
import tempfile
import core
import sys
import json
import os.path
from time import gmtime, strftime
import logging
from logging import config

log = core.LogExpert.getConsoleLogger();
 

if __name__ == '__main__':
	log.info('update begin, initiating..')

	config = core.ConfigExpert(log).getConfigFromFileOrInput()

	response = core.RatesExpert(log).getRatesAndPublish(
		config['open_exchange_app_id'], 
		config['firebase_auth_key'],
		config['firebase_app_id']
		)

	log.debug('Response:\n%s' % response)

	log.info('update end')

