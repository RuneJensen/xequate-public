from flask import Flask
import core

app = Flask(__name__)
app.config['DEBUG'] = True

# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

@app.route('/')
def hello():
    return 'Hello World!'

@app.route('/updateRates')
def updateRates():
    response = core.RatesExpert().getRatesAndPublish()
    return 'Rates updated: %s.' % response

@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, nothing at this URL.', 404
